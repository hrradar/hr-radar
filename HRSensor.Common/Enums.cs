﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSensor.Common
{
    public class Enums
    {
        public enum SurveyStatus
        {
            Outstanding,
            Completed
        }

        public enum Gender
        {
            MALE,
            FEMALE
        }

        public enum QuestionType
        {
            MULTIPLE_CHOICE,
            FREE_TEXT
        }

        public enum DifficultyLevel
        {
            NONE,
            EASY,
            NORMAL,
            HARD,
            IMPOSSIBLE
        }

        public enum Weight
        {
            W1 = 0,
            W2 = 25,
            W3 = 50,
            W4 = 75,
            W5 = 100
        }

        public enum MessageType
        {
            NOTIFICATION,
            QUESTION
        }

        public enum Lang
        {
            EN,
            GR
        }
    }
}
