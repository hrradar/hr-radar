﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSensor.Common
{
    public class Constants
    {
        public const string EMPLOYEE_ROLE = "Employee";
        public const string ARCHITECT_ROLE = "Architect";
        public const string HRMONITOR_ROLE = "HRMonitor";
    }
}
