﻿using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace HRSensor.Web.Helpers
{
    public class Mailer
    {
        public void SendMail(string recipient, string subject, string body)
        {
            try
            {
                var smtpServer = ConfigurationManager.AppSettings["SmtpServer"].ToString(); 
                var username = ConfigurationManager.AppSettings["Username"].ToString();
                var password = ConfigurationManager.AppSettings["Password"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["MailPort"]);
                var fromMail = ConfigurationManager.AppSettings["FromMail"];

                SmtpClient client = new SmtpClient(smtpServer, port)
                {
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(username, password),
                    EnableSsl = true
                };

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(fromMail);
                mailMessage.To.Add(recipient);
                mailMessage.Body = body;
                mailMessage.Subject = subject;
                client.Send(mailMessage);
            }
            catch
            {

            }
        }
    }
}