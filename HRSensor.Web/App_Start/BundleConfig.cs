﻿using System.Web;
using System.Web.Optimization;

namespace HRSensor.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminpro").Include(
                        "~/assets/plugins/jquery/jquery.min.js",
                        "~/assets/plugins/bootstrap/js/popper.min.js",
                        "~/assets/plugins/bootstrap/js/bootstrap.min.js",
                        "~/Scripts/js/perfect-scrollbar.jquery.min.js",
                        "~/Scripts/js/waves.js",
                        "~/Scripts/js/sidebarmenu.js",
                        "~/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js",
                        "~/assets/plugins/sparkline/jquery.sparkline.min.js",
                        "~/Scripts/js/custom.min.js",
                        "~/assets/plugins/toast-master/js/jquery.toast.js",
                        "~/assets/plugins/sweetalert/sweetalert.min.js",
                        "~/assets/plugins/styleswitcher/jQuery.style.switcher.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/HRRadarCSS").Include(
                      //"~/Content/bootstrap.css",
                      "~/assets/plugins/bootstrap/css/bootstrap.css",
                      "~/assets/plugins/toast-master/css/jquery.toast.css",
                      "~/assets/plugins/sweetalert/sweetalert.css",
                      "~/Content/css/style.css",
                      "~/Content/css/colors/blue.css",                      
                      "~/Content/scss/icons/material-design-iconic-font/css/materialdesignicons.min.css"
                      //"~/Content/site.css"
                      ));
        }
    }
}