﻿using AutoMapper;
using HRSensor.Models;
using HRSensor.Web.ViewModels;

namespace HRSensor.Web.App_Start
{
    public class AutoMapperConfig : AutoMapper.Profile
    {
        public AutoMapperConfig()
        {
            Mapper.CreateMap<User, UserViewModel>()
                .ForMember(sr => sr.Email, opt => opt.MapFrom(u => u.ContactDetails.Email));
            //Mapper.CreateMap<UserContactDetail, UserViewModel>();

            Mapper.CreateMap<UserViewModel, User>()
                .ForMember(sr => sr.ContactDetails, opt => opt.MapFrom(m => m));
            Mapper.CreateMap<UserViewModel, UserContactDetail>()
                .ForMember(sr => sr.Address, opt => opt.MapFrom(cd => cd.Address))
                .ForMember(sr => sr.Zip, opt => opt.MapFrom(cd => cd.Zip))
                .ForMember(sr => sr.Country, opt => opt.MapFrom(cd => cd.Country))
                .ForMember(sr => sr.Phone, opt => opt.MapFrom(cd => cd.Phone))
                .ForMember(sr => sr.Facebook, opt => opt.MapFrom(cd => cd.Facebook))
                .ForMember(sr => sr.Twitter, opt => opt.MapFrom(cd => cd.Twitter))
                .ForMember(sr => sr.Linkedin, opt => opt.MapFrom(cd => cd.Linkedin))
                .ForMember(sr => sr.Google, opt => opt.MapFrom(cd => cd.Google));


        }

        public static void RegisterMappings()
        {
           
        }
    }
}