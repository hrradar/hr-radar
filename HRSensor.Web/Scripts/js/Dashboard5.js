﻿//var myChart = echarts.init(document.getElementById('Peaks'));
var Chart = {
    drawChart: function (chart_data) {
        var myChart = echarts.init(document.getElementById('Peaks'));
        var app = {};
        option = null;

option = {
    tooltip: {
        formatter: "{a} <br/>{b} : {c}%"
    },
    toolbox: {
        show : true,
        feature: {
            restore: {},
            saveAsImage: {show : true}
        }
    },
    series: [
        {
            name: 'Current value',
            type: 'gauge',
            min: 0,
            max: 100,
            detail: { formatter: '{value}%' },
            data: [{ value: 50, name: 'Current Value' }],
            axisLine: {
                lineStyle: {
                    color: [[0.1, '#55ce63'], [0.3, '#ffbf00'], [0.5, '#ff8000'], [1, '#f62d51']],
                }
            }
        }
    ]
};



if (option && typeof option === "object") {
        myChart.setOption(option, true), $(function () {
            function resize() {
                setTimeout(function () {
                    myChart.resize()
                }, 100)
            }
            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
        });
};
