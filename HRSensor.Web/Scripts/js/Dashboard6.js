﻿var myChart = echarts.init(document.getElementById('Threshold'));

option = {

    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:  ["Hidddstory"]
    },
    toolbox: {
        show: true,
        feature: {
            saveAsImage: { show: true }
        }
    },
    color: ["#009efb"],
    calculable: true,
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            data: ['12/2017', '1/2018', '2/2018', '3/2018']
        }
    ],
    yAxis: [
        {
            type: 'value',
            axisLabel: {
                formatter: '{value}'
            }
        }
    ],

    series: [
        {
            name: "Threshdddold",
            type: 'line',
            color: ['#000'],
            data: [0,50,100,50],
            itemStyle: {
                normal: {
                    lineStyle: {
                        shadowColor: 'rgba(0,0,0,0.3)',
                        shadowBlur: 10,
                        shadowOffsetX: 8,
                        shadowOffsetY: 8
                    }
                }
            }
        }
    ]
};



 if (option && typeof option === "object") {
    myChart.setOption(option, true), $(function () {
        function resize() {
            setTimeout(function () {
                myChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });
}