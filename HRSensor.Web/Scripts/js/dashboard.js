﻿var Chart = {
    drawChart: function (chart_data) {
        var myChart = echarts.init(document.getElementById(chart_data.idelement));
        var app = {};
        option = null;

        switch (chart_data.type) {
            case 'cprs_history':
            case 'job_satisfaction_history':
            case 'threshold_history':       
                option = {

                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: chart_data.legend_data
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true }
                        }
                    },
                    color: ["#009efb"],
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: chart_data.x_axis_data
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            axisLabel: {
                                formatter: '{value}'
                            }
                        }
                    ],

                    series: [
                        {
                            name: chart_data.name,
                            type: 'line',
                            color: ['#000'],
                            data: chart_data.series_data,
                            itemStyle: {
                                normal: {
                                    lineStyle: {
                                        shadowColor: 'rgba(0,0,0,0.3)',
                                        shadowBlur: 10,
                                        shadowOffsetX: 8,
                                        shadowOffsetY: 8
                                    }
                                }
                            }
                        }
                    ]
                };
                break;
            case 'threshold':
            case 'cprs':
                // specify chart configuration item and data
              if(chart_data.type == 'cprs')
                {
                option = {
                    tooltip: {
                        formatter: "{a} <br/>{b} : {c}"
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true }
                        }
                    },

                    series: [
                        {
                            name: chart_data.name,
                            type: 'gauge',
                            min: 0,
                            max: 20,
                            detail: { formatter: '{value}' },
                            data: chart_data.series_data,
                            axisLine: {
                                lineStyle: {
                                    color: [[0.1, '#55ce63'], [0.3, '#ffbf00'], [0.5, '#ff8000'], [1, '#f62d51']],
                                }
                            }
                        }
                    ]
                };
          break;
                 }
               else
                {
      
                option = {
                    tooltip: {
                        formatter: "{a} <br/>{b} : {c}"
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true }
                        }
                    },

                    series: [
                        {
                            name: chart_data.name,
                            type: 'gauge',
                            min: 0,
                            max: 100,
                            detail: { formatter: '{value}' },
                            data: chart_data.series_data,
                            axisLine: {
                                lineStyle: {
                                    color: [[0.1, '#55ce63'], [0.3, '#ffbf00'], [0.5, '#ff8000'], [1, '#f62d51']],
                                }
                            }
                        }
                    ]
                };
                break;
               }
            case 'job_satisfaction':
                option = {
                    title: {
                        text: '',
                        subtext: '',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                        data: chart_data.legend_data
                    },
                    series: [
                        {
                            name: chart_data.name,
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '50%'],
                            data: chart_data.series_data,
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                break;
            case 'health_wellbeing':
                option = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['General Health Perception', 'Mental Health And Well Being']
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true }
                        }
                    },
                    color: ["#55ce63", "#009efb"],
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            data: ['12/2017', '1/2018', '2/2018', '3/2018']
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: 'General Health Perception',
                            type: 'bar',
                            data: chart_data.series_data[0],
                            markPoint: {
                                data: chart_data.mark_points[0]
                            },
                            markLine: {
                                data: [
                                    { type: 'average', name: 'Average' }
                                ]
                            }
                        },
                        {
                            name: 'Mental Health And Well Being',
                            type: 'bar',
                            data: chart_data.series_data[1],
                            markPoint: {
                                data: 
                                    chart_data.mark_points[1]                                
                            },
                            markLine: {
                                data: [
                                    { type: 'average', name: 'Average' }
                                ]
                            }
                        }
                    ]
                };
               break;
                 


        }



        if (option && typeof option === "object") {
            myChart.setOption(option, true), $(function () {
                function resize() {
                    setTimeout(function () {
                        myChart.resize()
                    }, 100)
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
            });
        }
    }
}
