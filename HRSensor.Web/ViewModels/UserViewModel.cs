﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Web.ViewModels
{
    public class UserViewModel
    {
        public long UserId { get; set; }
        [Required(ErrorMessage = "Username is required.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "LastName is required.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "FirstName is required.")]
        //public string Password { get; set; }
        //[Required(ErrorMessage = "Password is required.")]
        //public string ConfirmPassword { get; set; }
        //[Required(ErrorMessage = "ConfirmPassword is required.")]
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        [Required(ErrorMessage = "Gender is required.")]
        public Gender Gender { get; set; }
        [Required(ErrorMessage = "BirthDate is required.")]
        public DateTime? BirthDate { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "HRCode is required.")]
        public string HRCode { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Google { get; set; }
        public string Linkedin { get; set; }
    }
}