﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.ViewModels
{
    public class CalculatorViewModel
    {
        public List<float> ComputedValues { get; set; }
    }
}