﻿using HRSensor.Models;
using HRSensor.Web.PocoObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.ViewModels
{
    public class DashboardViewModel
    {
        public int CompletedInterviews { get; set; }
        public int InterviewsInProgress { get; set; }
        public int TotalQuestionnairies { get; set; }
        public int InvitedInLastInterview { get; set; }
        public Dictionary<string, BaseChart> ChartsData { get; set; }
        public List<ThresholdCharts> ThresholdCharts { get; set; }
    }
}