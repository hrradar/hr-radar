﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Web.ViewModels
{
    public class AssignmentViewModel
    {
        public long UserId { get; set; }
        public string FullName { get; set; }
        public string HRCode { get; set; }
        public bool IsSelected { get; set; }
    }
}