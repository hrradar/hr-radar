﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.ViewModels
{
    public class AssignmentResultsViewModel
    {
        public long SurveyId { get; set; }
        public long[] UserIds { get; set; }
    }
}