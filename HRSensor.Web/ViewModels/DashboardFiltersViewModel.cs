﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.ViewModels
{
    public class DashboardFiltersViewModel
    {
        public List<long> QuestionSectors { get; set; }
        public long SurveyId { get; set; }
    }
}