﻿using HRSensor.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRSensor.Web.Controllers
{
    [Authorize(Roles = Constants.ARCHITECT_ROLE + ", " + Constants.HRMONITOR_ROLE)]
    public class FeedbackController : Controller
    {
        // GET: Feedback
        public ActionResult News()
        {
            return View();
        }

        public ActionResult Notifications()
        {
            return View();
        }
    }
}