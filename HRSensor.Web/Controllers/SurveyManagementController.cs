﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using static HRSensor.Common.Enums;
using HRSensor.Models;
using HRSensor.Common;
using HRSensor.Repositories.Interfaces;

namespace HRSensor.Web.Controllers
{
    [Authorize(Roles = Constants.ARCHITECT_ROLE + ", " + Constants.HRMONITOR_ROLE)]
    public class SurveyManagementController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IUserRepo _userRepo;

        public SurveyManagementController(IUserRepo userRepo) : base(userRepo)
        {
            _userRepo = userRepo;
        }
        // GET: surveys
        public ActionResult Index(string txtDateCreatedFrom, string txtDateCreatedTo, int? cmbUser)
        {

            //List<User> objUslist = (from data in db.User.OrderBy(x => x.FullName).Where(x => x.IsActive == true)
            //                                select data).ToList();
            var loguser = GetUserByAspNetUserId();
            List<User> objUslist = _userRepo.GetAuthors(loguser.OrganizationId);
            User objUs = new User();
            objUs.FullName = "-- Select User --";
            objUs.UserId = 0;
            objUslist.Insert(0, objUs);
            SelectList objUsdata = new SelectList(objUslist, "UserId", "FullName", 0);
            ViewBag.Users = objUsdata;

            var surveys = db.Surveys.Where(c => c.IsActive == true);

            if (!String.IsNullOrEmpty(txtDateCreatedFrom))
            {
                DateTime dtFrom = Convert.ToDateTime(txtDateCreatedFrom);
                surveys = surveys.Where(c => c.CreationDate >= dtFrom);
            }
            if (!String.IsNullOrEmpty(txtDateCreatedTo))
            {
                DateTime dtTo = Convert.ToDateTime(txtDateCreatedTo);
                surveys = surveys.Where(c => c.CreationDate <= dtTo);
            }
            if (cmbUser != 0 && cmbUser != null)
            {
                surveys = surveys.Where(c => c.UserId == cmbUser);
            }

            ModelState.Clear();
            return View(surveys.ToList());
        }

        // GET: surveys/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = db.Surveys.Find(id);
            if (survey == null)
            {
                return HttpNotFound();
            }
            
            return View(survey);
        }

        //GET: surveys/Create
        public ActionResult Create()
        {
            ViewBag.BusinessUnits = new SelectList(db.BusinessUnits.OrderBy(x => x.BusinessUnitName).Where(x => x.IsActive == true), "BusinessUnitId", "BusinessUnitName");
            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle");
            ViewBag.Departments = new SelectList(db.Departments.OrderBy(x => x.DepartmentName).Where(x => x.IsActive == true), "DepartmentId", "DepartmentName");
            ViewBag.Organizations = new SelectList(db.Organizations.OrderBy(x => x.OrganizationName).Where(x => x.IsActive == true), "OrganizationId", "OrganizationName");
            if (HttpContext.Request.IsAjaxRequest())
                return Json(ViewBag, JsonRequestBehavior.AllowGet);
            return View();
        }

        //POST: surveys/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SurveyId, SurveyTitle, SurveyCode, OrganizationId, BusinessUnitId, QuestionnaireId, DepartmentId, GeoLocationId, StartDate, EndDate, Status, UserId, CreationDate, CPRS, TimeNeeded, HasHint, WithReview, MandatoryAnswersToAllQuestions, NoOfQuestionsPerPage, WithResultPage, CalculateDistinctWeighting, IsActive")] Survey survey)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (survey.OrganizationId != 0)
                    {
                        var orName = db.Organizations.Find(survey.OrganizationId);
                        survey.Organization = orName;
                    }

                    if (survey.BusinessUnitId != 0 && survey.BusinessUnitId != null)
                    {
                        var buName = db.BusinessUnits.Find(survey.BusinessUnitId);
                        survey.BusinessUnit = buName;
                    }

                    if (survey.DepartmentId != 0 && survey.DepartmentId != null)
                    {
                        var dName = db.Departments.Find(survey.DepartmentId);
                        survey.Department = dName;
                    }

                    if (survey.GeoLocationId != 0 && survey.GeoLocationId != null)
                    {
                        var glName = db.GeoLocations.Find(survey.GeoLocationId);
                        survey.GeoLocation = glName;
                    }

                    var user = GetUserByAspNetUserId();
                    //survey.Author = user;
                    survey.UserId = user.UserId;
                    
                    var qnName = db.Questionnaires.Find(survey.QuestionnaireId);
                    survey.Questionnaire = qnName;
                    survey.CreationDate = DateTime.Now;
                    survey.IsActive = true;
                    survey.Status = SurveyStatus.Outstanding;
                    
                    db.Surveys.Add(survey);

                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            catch (Exception exception)
            {
                var ex = exception.Message;
            }

            ViewBag.BusinessUnits = new SelectList(db.BusinessUnits.OrderBy(x => x.BusinessUnitName).Where(x => x.IsActive == true), "BusinessUnitId", "BusinessUnitName", survey.BusinessUnitId);
            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", survey.QuestionnaireId);
            ViewBag.Departments = new SelectList(db.Departments.OrderBy(x => x.DepartmentName).Where(x => x.IsActive == true), "DepartmentId", "DepartmentName", survey.DepartmentId);
            ViewBag.Organizations = new SelectList(db.Organizations.OrderBy(x => x.OrganizationName).Where(x => x.IsActive == true), "OrganizationId", "OrganizationName", survey.OrganizationId);

            return View(survey);
        }


        // GET: surveys/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = db.Surveys.Find(id);
            if (survey == null)
            {
                return HttpNotFound();
            }

            ViewBag.BusinessUnits = new SelectList(db.BusinessUnits.OrderBy(x => x.BusinessUnitName).Where(x => x.IsActive == true), "BusinessUnitId", "BusinessUnitName", survey.BusinessUnitId);
            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", survey.QuestionnaireId);
            ViewBag.Departments = new SelectList(db.Departments.OrderBy(x => x.DepartmentName).Where(x => x.IsActive == true), "DepartmentId", "DepartmentName", survey.DepartmentId);
            ViewBag.Organizations = new SelectList(db.Organizations.OrderBy(x => x.OrganizationName).Where(x => x.IsActive == true), "OrganizationId", "OrganizationName", survey.OrganizationId);
           
            return View(survey);
        }

        // POST: surveys/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SurveyId, SurveyTitle, SurveyCode, OrganizationId, BusinessUnitId, QuestionnaireId, DepartmentId, GeoLocationId, StartDate, EndDate, Status, UserId, CreationDate, CPRS, TimeNeeded, HasHint, WithReview, MandatoryAnswersToAllQuestions, NoOfQuestionsPerPage, WithResultPage, CalculateDistinctWeighting, IsActive")] Survey survey)
        {
            if (ModelState.IsValid)
            {
                var surv = db.Surveys.Find(survey.SurveyId);

                surv.QuestionnaireId = survey.QuestionnaireId;
                surv.SurveyTitle = survey.SurveyTitle;
                surv.SurveyCode = survey.SurveyCode;
                surv.OrganizationId = survey.OrganizationId;
                surv.BusinessUnitId = survey.BusinessUnitId;
                surv.DepartmentId = survey.DepartmentId;
                surv.GeoLocationId = survey.GeoLocationId;
                surv.StartDate = survey.StartDate;
                surv.EndDate = survey.EndDate;
                surv.TimeNeeded = survey.TimeNeeded;
                surv.NoOfQuestionsPerPage = survey.NoOfQuestionsPerPage;
                surv.HasHint = survey.HasHint;
                surv.WithReview = survey.WithReview;
                surv.MandatoryAnswersToAllQuestions = survey.MandatoryAnswersToAllQuestions;
                surv.WithResultPage = survey.WithResultPage;
                surv.CalculateDistinctWeighting = survey.CalculateDistinctWeighting;

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BusinessUnits = new SelectList(db.BusinessUnits.OrderBy(x => x.BusinessUnitName).Where(x => x.IsActive == true), "BusinessUnitId", "BusinessUnitName", survey.BusinessUnitId);
            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", survey.QuestionnaireId);
            ViewBag.Departments = new SelectList(db.Departments.OrderBy(x => x.DepartmentName).Where(x => x.IsActive == true), "DepartmentId", "DepartmentName", survey.DepartmentId);
            ViewBag.Organizations = new SelectList(db.Organizations.OrderBy(x => x.OrganizationName).Where(x => x.IsActive == true), "OrganizationId", "OrganizationName", survey.OrganizationId);

            return View(survey);
        }

        // GET: surveys/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = db.Surveys.Find(id);
            if (survey == null)
            {
                return HttpNotFound();
            }

            ViewBag.BusinessUnits = new SelectList(db.BusinessUnits.OrderBy(x => x.BusinessUnitName).Where(x => x.IsActive == true), "BusinessUnitId", "BusinessUnitName", survey.BusinessUnitId);
            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", survey.QuestionnaireId);
            ViewBag.Departments = new SelectList(db.Departments.OrderBy(x => x.DepartmentName).Where(x => x.IsActive == true), "DepartmentId", "DepartmentName", survey.DepartmentId);
            ViewBag.Organizations = new SelectList(db.Organizations.OrderBy(x => x.OrganizationName).Where(x => x.IsActive == true), "OrganizationId", "OrganizationName", survey.OrganizationId);

            return View(survey);
        }

        //// POST: surveys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Survey survey = db.Surveys.Find(id);
            survey.IsActive = false;
            db.Entry(survey).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
