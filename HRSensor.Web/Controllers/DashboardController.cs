﻿using HRRadar.Calculator;
using HRSensor.Common;
using HRSensor.Models;
using HRSensor.Repositories.Interfaces;
using HRSensor.Web.PocoObjects;
using HRSensor.Web.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HRSensor.Web.Controllers
{
    [Authorize(Roles = Constants.ARCHITECT_ROLE + ", " + Constants.HRMONITOR_ROLE)]
    public class DashboardController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ICalculator _calculator;
        private static Random random = new Random();
        private static DashboardViewModel dashboardViewModel;

        public DashboardController(ICalculator calculator, IUserRepo userRepo) : base(userRepo)
        {
            _calculator = calculator;
        }
        
        public ActionResult Index()
        {
            var dashboardViewModel = new DashboardViewModel();

            var surveys = db.Surveys.Where(s => s.IsActive == true).OrderBy(s => s.SurveyTitle);

            List<SelectListItem> surveysListItems = new List<SelectListItem>();
            
            if (surveys.Any())
            {
                foreach(Survey survey in surveys)
                {
                    surveysListItems.Add(new SelectListItem { Text = survey.SurveyTitle, Value = survey.SurveyId.ToString() });
                }
            }

            ViewBag.Interviews = surveysListItems;

            return View(dashboardViewModel);
        }

        public ActionResult Charts(string surveyId, string questionSectors)
        {
            DashboardFiltersViewModel dashboardFilters = new DashboardFiltersViewModel();

            try
            {
                dashboardFilters.SurveyId = long.Parse(surveyId);
            }catch(Exception e)
            {
                //Handle the exception here.
            }

            if (!string.IsNullOrEmpty(questionSectors))
            {
                dashboardFilters.QuestionSectors = Array.ConvertAll(questionSectors.Split(','), long.Parse).ToList();
            }

            dashboardViewModel = GetDashboardViewModel(dashboardFilters);
            
            return PartialView("Charts",dashboardViewModel);
        }

        public JsonResult GetCharts(DashboardFiltersViewModel dashboardFilters)
        {
            return Json(dashboardViewModel, JsonRequestBehavior.AllowGet);
        }

        private DashboardViewModel GetDashboardViewModel(DashboardFiltersViewModel dashboardFilters)
        {
            var dashboardViewModel = new DashboardViewModel();

            dashboardViewModel.CompletedInterviews = random.Next(50, 100);
            dashboardViewModel.InterviewsInProgress = random.Next(50);
            dashboardViewModel.TotalQuestionnairies = random.Next(200);
            dashboardViewModel.InvitedInLastInterview = random.Next(20);

            dashboardViewModel.ChartsData = new Dictionary<string, PocoObjects.BaseChart>();

            var companySectorAnswers = GetCompanySectorAnswers(dashboardFilters);

            var cprsData = GetCprsData(dashboardFilters, companySectorAnswers);
            if(cprsData.Count > 0)
            {
                dashboardViewModel.ChartsData.Add("cprs", ChartUtils.GetCprsChart(cprsData[0]));
                dashboardViewModel.ChartsData.Add("cprs_history", ChartUtils.GetCprsHistoryChart(GetCprsLineChartData(dashboardFilters.SurveyId)));
            }
            else
            {
                dashboardViewModel.ChartsData.Add("cprs", ChartUtils.getInstance().getChart("cprs"));
                dashboardViewModel.ChartsData.Add("cprs_history", ChartUtils.getInstance().getChart("cprs_history"));
            }
            dashboardViewModel.ChartsData.Add("job_satisfaction", ChartUtils.getInstance().getChart("job_satisfaction"));
            dashboardViewModel.ChartsData.Add("job_satisfaction_history", ChartUtils.getInstance().getChart("job_satisfaction_history"));
            dashboardViewModel.ChartsData.Add("health_wellbeing", ChartUtils.getInstance().getChart("health_wellbeing"));
            dashboardViewModel.ThresholdCharts = GetThresholdCharts(dashboardFilters, companySectorAnswers);

            return dashboardViewModel;
        }

        private List<ThresholdCharts> GetThresholdCharts(DashboardFiltersViewModel dashboardFilters, List<CompanySectorAnswerTitled> companySectorAnswers)
        {
            List<ThresholdCharts> thresholdChartsList = new List<ThresholdCharts>();
            if(dashboardFilters.QuestionSectors != null)
            {
                if(companySectorAnswers.Count > 0)
                {
                    foreach (CompanySectorAnswerTitled companySectorAnswer in companySectorAnswers)
                    {
                        ThresholdCharts thresholdCharts = new ThresholdCharts();
                        thresholdCharts.QuestionSectorTitle = companySectorAnswer.Title;
                        thresholdCharts.QuestionSectorId = companySectorAnswer.QuestionSectorId;
                        thresholdCharts.MeterChart = ChartUtils.GetMeterChart(companySectorAnswer);
                        thresholdCharts.ProgressBar = companySectorAnswer.MathAvg;
                        thresholdCharts.LineChart = ChartUtils.GetLineChart(GetThresholdLineChartData(companySectorAnswer.SurveyId, companySectorAnswer.QuestionSectorId));
                        thresholdChartsList.Add(thresholdCharts);
                    }
                }
                else
                {
                    var questionSectors = GetQuestionSectors(dashboardFilters.QuestionSectors);
                    foreach(QuestionSector questionSector in questionSectors)
                    {
                        ThresholdCharts thresholdCharts = new ThresholdCharts();
                        thresholdCharts.QuestionSectorTitle = questionSector.QuestionSectorName;
                        thresholdCharts.QuestionSectorId = questionSector.QuestionSectorId;
                        thresholdCharts.MeterChart = ChartUtils.GetMeterChart(null);
                        thresholdCharts.ProgressBar = 0;
                        thresholdCharts.LineChart = ChartUtils.GetLineChart(null);
                        thresholdChartsList.Add(thresholdCharts);
                    }
                }
               
            }

            return thresholdChartsList;
        }

        private List<float> GetCprsData(DashboardFiltersViewModel dashboardFilters, List<CompanySectorAnswerTitled> companySectorAnswers)
        {
            List<float> cprsData = new List<float>();
            //TODO quick fix for release. fix this later to be independent from questionsectors
            DashboardFiltersViewModel cloneDashboardFilters = new DashboardFiltersViewModel();
            cloneDashboardFilters.SurveyId = dashboardFilters.SurveyId;
            cloneDashboardFilters.QuestionSectors = null;
            companySectorAnswers = GetCompanySectorAnswers(cloneDashboardFilters);
            if (companySectorAnswers.Any())
            {
                float CPRI = 0;
                foreach (CompanySectorAnswerTitled companySectorAnswer in companySectorAnswers)
                {
                    CPRI += companySectorAnswer.CPRI;
                }
                cprsData.Add(CPRI);
            }
            
            return cprsData;
        }

        private LineChartData GetCprsLineChartData(long surveyId)
        {
            LineChartData data = new LineChartData();
            data.SeriesData = new List<float>();
            data.XAxisData = new List<string>();

            List<Survey> surveys = (from s in db.Surveys
                                    where s.SurveyId == surveyId && s.IsActive == true
                                    select s).ToList();

            foreach (Survey survey in surveys)
            {
                var results =  (from s in db.Surveys
                               join csa in db.CompanySectorAnswers
                               on s.SurveyId equals csa.SurveyId
                               where s.QuestionnaireId == survey.QuestionnaireId
                               orderby s.EndDate
                               select new
                               {
                                   CPRI = csa.CPRI,
                                   EndDate = s.EndDate
                               }).ToList();

                SortedDictionary<string, float> values = new SortedDictionary<string, float>();
                foreach (var result in results)
                {
                    string endDate = ((DateTime)result.EndDate).ToString("MM/yyyy");
                    if (!values.ContainsKey(endDate))
                    {
                        values.Add(endDate, result.CPRI);
                    }
                    else
                    {
                        values[endDate] = values[endDate] + result.CPRI;
                    }
                }

                foreach(KeyValuePair<string, float> kvp in values)
                {
                    data.SeriesData.Add(kvp.Value);
                    data.XAxisData.Add(kvp.Key);
                }
            }

            return data;
        }

            private LineChartData GetThresholdLineChartData(long surveyId, long questionSectorId)
        {
            LineChartData data = new LineChartData();
            data.SeriesData = new List<float>();
            data.XAxisData = new List<string>();

            List<Survey> surveys = (from s in db.Surveys
                          where s.SurveyId == surveyId && s.IsActive == true
                          select s).ToList();

            foreach (Survey survey in surveys)
            {
                var results = (from s in db.Surveys
                               join csa in db.CompanySectorAnswers
                               on s.SurveyId equals csa.SurveyId
                               where csa.QuestionSectorId == questionSectorId && s.QuestionnaireId == survey.QuestionnaireId
                               orderby s.EndDate
                               select new
                               {
                                   MathAvg = csa.MathAvg,
                                   EndDate = s.EndDate
                               }).ToList();

                foreach (var result in results)
                {
                    data.SeriesData.Add(result.MathAvg);
                    data.XAxisData.Add(((DateTime)result.EndDate).ToString("MM/yyyy"));
                }
            }

            return data;
        }

        private List<CompanySectorAnswerTitled> GetCompanySectorAnswers(DashboardFiltersViewModel dashboardFilters)
        {
            var organizationId = GetUserByAspNetUserId().OrganizationId;
            var questionSectors = dashboardFilters.QuestionSectors;
            var surveyId = dashboardFilters.SurveyId;

            IQueryable<CompanySectorAnswerTitled> companySectorAnswers;
            if(questionSectors != null)
            {
                companySectorAnswers = from companySector in db.CompanySectorAnswers
                                       join questionSector in db.QuestionSectors
                                       on companySector.QuestionSectorId equals questionSector.QuestionSectorId
                                       where companySector.SurveyId == surveyId &&
                                               companySector.OrganizationId == organizationId &&
                                               questionSectors.Contains(companySector.QuestionSectorId)
                                       select new CompanySectorAnswerTitled
                                       {
                                           CompanySectorAnswerId = companySector.CompanySectorAnswerId,
                                           CPRI = companySector.CPRI,
                                           MathAvg = companySector.MathAvg,
                                           OrganizationId = companySector.OrganizationId,
                                           QuestionSectorId = companySector.QuestionSectorId,
                                           SurveyId = companySector.SurveyId,
                                           Title = questionSector.QuestionSectorName
                                       };

            }
            else
            {
                companySectorAnswers = from companySector in db.CompanySectorAnswers
                                       join questionSector in db.QuestionSectors
                                       on companySector.QuestionSectorId equals questionSector.QuestionSectorId
                                       where companySector.SurveyId == surveyId &&
                                               companySector.OrganizationId == organizationId
                                       select new CompanySectorAnswerTitled
                                       {
                                           CompanySectorAnswerId = companySector.CompanySectorAnswerId,
                                           CPRI = companySector.CPRI,
                                           MathAvg = companySector.MathAvg,
                                           OrganizationId = companySector.OrganizationId,
                                           QuestionSectorId = companySector.QuestionSectorId,
                                           SurveyId = companySector.SurveyId,
                                           Title = questionSector.QuestionSectorName
                                       };

            }

            if (companySectorAnswers.Any())
            {
                return companySectorAnswers.ToList();
            }
         
            return new List<CompanySectorAnswerTitled>();
        }

        private List<QuestionSector> GetQuestionSectors(List<long> questionSectors)
        {
            var result = from qs in db.QuestionSectors
                         where questionSectors.Contains(qs.QuestionSectorId)
                         select qs;

            if (result.Any())
            {
                return result.ToList();
            }

            return new List<QuestionSector>();
        }
    }
}