﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using HRSensor.Web.ViewModels;
using HRSensor.Models;
using HRSensor.Web.Models;
using HRSensor.Common;
using HRSensor.Repositories.Interfaces;
using HRSensor.Web.Filters;
using System.Data.Entity;

namespace HRSensor.Web.Controllers
{
    [Authorize(Roles = Constants.ARCHITECT_ROLE + ", " + Constants.HRMONITOR_ROLE)]
    public class UsersController : BaseController
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IUserRepo _userRepo;

        public UsersController(ApplicationDbContext dbContext, IUserRepo userRepo)
        {
            _dbContext = dbContext;
            _userRepo = userRepo;
        }

        public ActionResult Index()
        {
            var dbUsers = _dbContext.User.Where(u => u.IsActive == true).Include(u => u.ContactDetails).ToList();
            List<UserViewModel> uVms = new List<UserViewModel> ();
            foreach (var u in dbUsers)
            {
                UserViewModel uVm = new UserViewModel();
                uVm.UserId = u.UserId;
                uVm.LastName = u.LastName;
                uVm.FirstName = u.FirstName;
                uVm.Email = u.ContactDetails.Email;
                uVms.Add(uVm);
            }
               
            //var users = Mapper.Map<List<UserViewModel>>(dbUsers);
            
            return View(uVms);
        }

        [RestoreModelStateFromTempData]
        public ActionResult Create(bool? userCreated)
        {
            if (ModelState.IsValid)
                ModelState.Clear();

            ViewBag.UserCreated = false;
            if (userCreated.HasValue && (bool)userCreated)
            {
                ViewBag.UserCreated = true;
            }
            
            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(User user)
        //{            
        //    _dbContext.User.Add(user);
            
        //    return View(user);
        //}

        public ActionResult Edit(int id)
        {            
            var user = _userRepo.GetUserByUserId(id);
            var aspNetUser = _userRepo.GetApplicationUserByAspNetUserId(user.AspNetUserId);

            UserViewModel userVm = new UserViewModel();
            userVm.Address = user.ContactDetails.Address;
            userVm.BirthDate = user.BirthDate;
            userVm.City = user.ContactDetails.City;
            userVm.Country = user.ContactDetails.Country;
            userVm.Email = user.ContactDetails.Email;
            userVm.Facebook = user.ContactDetails.Facebook;
            userVm.FatherName = user.FatherName;
            userVm.FirstName = user.FirstName;
            userVm.Gender = user.Gender;
            userVm.Google = user.ContactDetails.Google;
            userVm.HRCode = user.HRCode;
            userVm.LastName = user.LastName;
            userVm.Linkedin = user.ContactDetails.Linkedin;
            userVm.Mobile = user.ContactDetails.Mobile;
            userVm.Phone = user.ContactDetails.Phone;
            userVm.Twitter = user.ContactDetails.Twitter;
            userVm.UserId = user.UserId;
            userVm.Username = aspNetUser.UserName;
            userVm.Zip = user.ContactDetails.Zip;

            return View(userVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel userVm)
        {
            ViewBag.UserUpdated = false;           

            //var user = Mapper.Map<UserViewModel, User>(userVM);

            User user = new HRSensor.Models.User();
            user.ContactDetails = new UserContactDetail();
            
            user.ContactDetails.Address = userVm.Address;
            user.BirthDate = userVm.BirthDate;
            user.ContactDetails.City = userVm.City;
            user.ContactDetails.Country = userVm.Country;
            user.ContactDetails.Email = userVm.Email;
            user.ContactDetails.Facebook = userVm.Facebook;
            user.FatherName = userVm.FatherName;
            user.FirstName = userVm.FirstName;
            user.Gender = userVm.Gender;
            user.ContactDetails.Google = userVm.Google;
            user.HRCode = userVm.HRCode;
            user.LastName = userVm.LastName;
            user.ContactDetails.Linkedin = userVm.Linkedin;
            user.ContactDetails.Mobile = userVm.Mobile;
            user.ContactDetails.Phone = userVm.Phone;
            user.ContactDetails.Twitter = userVm.Twitter;
            user.UserId = userVm.UserId;
            user.ContactDetails.Zip = userVm.Zip;

            var updatedUser = _userRepo.UpdateUser(user);

            //var updatedUserVm = Mapper.Map<User, UserViewModel>(updatedUser);
            //updatedUserVm.Username = userVM.Username;
            //updatedUserVm.Email = userVM.Email;     // needs to be fixed...we store emai on AspNetUsers table
            
            if (updatedUser != null)
            {
                ViewBag.UserUpdated = true;
            }
            return RedirectToAction("Index");
            //return View(updatedUserVm);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var deleted = _userRepo.DeleteUser(id);

            return Json(new { UserDeleted = deleted }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int id)
        {            
            var user = _dbContext.User.Where(u => u.UserId == id).Include(u => u.ContactDetails).FirstOrDefault();
            var aspNetUser = _userRepo.GetApplicationUserByAspNetUserId(user.AspNetUserId);

            if (user == null)
            {
                return HttpNotFound();
            }
            UserViewModel userVm = new UserViewModel();
            userVm.Address = user.ContactDetails.Address;
            userVm.BirthDate = user.BirthDate;
            userVm.City = user.ContactDetails.City;
            userVm.Country = user.ContactDetails.Country;
            userVm.Email = user.ContactDetails.Email;
            userVm.Facebook = user.ContactDetails.Facebook;
            userVm.FatherName = user.FatherName;
            userVm.FirstName = user.FirstName;
            userVm.Gender = user.Gender;
            userVm.Google = user.ContactDetails.Google;
            userVm.HRCode = user.HRCode;
            userVm.LastName = user.LastName;
            userVm.Linkedin = user.ContactDetails.Linkedin;
            userVm.Mobile = user.ContactDetails.Mobile;
            userVm.Phone = user.ContactDetails.Phone;
            userVm.Twitter = user.ContactDetails.Twitter;
            userVm.UserId = user.UserId;
            userVm.Username = aspNetUser.UserName;
            userVm.Zip = user.ContactDetails.Zip;
            //var userVm = Mapper.Map<User, UserViewModel>(user);

            return View(userVm);
        }


    }
}