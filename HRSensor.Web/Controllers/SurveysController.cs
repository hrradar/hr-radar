﻿using System;
//using System.Collections.Generic;
using System.Data;
//using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using HRSensor.Models;
using HRSensor.Common;
using System.Collections.Generic;
using HRSensor.Web.ViewModels;
using HRSensor.Repositories.Interfaces;
using System.Data.Entity;
using HRSensor.Web.Helpers;
using System.Threading.Tasks;

namespace HRSensor.Web.Controllers
{
    [Authorize(Roles = Constants.ARCHITECT_ROLE + ", " + Constants.HRMONITOR_ROLE)]
    public class SurveysController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IUserRepo _userRepo;
        private readonly ISurveyRepo _surveyRepo;
        private readonly Mailer mailer = new Mailer();

        public SurveysController(IUserRepo userRepo, ISurveyRepo surveyRepo) : base(userRepo)
        {
            _userRepo = userRepo;
            _surveyRepo = surveyRepo;
        }

        // GET: surveys
        public ActionResult Index(string txtDateCreatedFrom, string txtDateCreatedTo)
        {
            var surveys = db.Surveys.Where(c => c.IsActive == true);

            if (!String.IsNullOrEmpty(txtDateCreatedFrom))
            {
                DateTime dtFrom = Convert.ToDateTime(txtDateCreatedFrom);
                surveys = surveys.Where(c => c.CreationDate >= dtFrom);
            }
            if (!String.IsNullOrEmpty(txtDateCreatedTo))
            {
                DateTime dtTo = Convert.ToDateTime(txtDateCreatedTo);
                surveys = surveys.Where(c => c.CreationDate <= dtTo);
            }

            ModelState.Clear();
            return View(surveys.ToList());
        }

        // GET: surveys/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = db.Surveys.Find(id);
            if (survey == null)
            {
                return HttpNotFound();
            }
            
            return View(survey);
        }
        //GET: Users/Details/5
        public ActionResult Assignments(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = db.Surveys.Find(id);
            if (survey == null)
            {
                return HttpNotFound();
            }

            var user = GetUserByAspNetUserId();
            List<AssignmentViewModel> asms = new List<AssignmentViewModel>();

            var us = _userRepo.GetUsersForAssignment(survey.SurveyId, survey.OrganizationId);

            foreach (User Usr in us)
            {
                AssignmentViewModel asm = new AssignmentViewModel();
                asm.UserId = Usr.UserId;
                asm.HRCode = Usr.HRCode;
                asm.FullName = Usr.FullName;
                asm.IsSelected = false;
                asms.Add(asm);
            };

            
            ViewBag.SurveyId = survey.SurveyId;

            return View(asms);
        }

        // POST: surveys users assignments
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveAssignments(AssignmentResultsViewModel results)
        {
            if (results.UserIds != null)
            {
                foreach (long usrId in results.UserIds)
                {
                    UserSurvey us = new UserSurvey();
                    us.SurveyId = results.SurveyId;
                    us.UserId = usrId;
                    us.IsActive = true;
                    us.Status = 0;
                    db.UserSurveys.Add(us);
                }


                db.SaveChanges();

                await AddNotificationsForEmployeesAsync(results.UserIds, results.SurveyId);
                SendMailToEmployees(results.UserIds);
            }

            //return RedirectToAction("Index");
            return Json(new { result = "Redirect", url = Url.Action("Index", "Surveys") });
        }

        
        //GET: Users/Details/5
        public ActionResult ViewAssignments(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = db.Surveys.Find(id);
            if (survey == null)
            {
                return HttpNotFound();
            }

            var usSu = db.UserSurveys.Where(x => x.SurveyId == survey.SurveyId).Include(x => x.User);
            List<AssignmentViewModel> asms = new List<AssignmentViewModel>();
            foreach (var uss in usSu)
            {
                AssignmentViewModel asm = new AssignmentViewModel();
                asm.UserId = uss.User.UserId;
                asm.HRCode = uss.User.HRCode;
                asm.FullName = uss.User.FullName;
                asms.Add(asm);
            };
            ViewBag.SurveyId = survey.SurveyId;
            ViewBag.SurveyStatus = survey.Status;
            return View(asms);
        }

        public ActionResult DeleteAssignments(int? userId, int? surveyId)
        {
            if (userId != null && surveyId != null)
            {
                UserSurvey us = db.UserSurveys.Where(x => x.SurveyId == surveyId && x.UserId == userId).FirstOrDefault();
                if (us == null)
                {
                    return HttpNotFound();
                }
                db.UserSurveys.Remove(us);
                db.SaveChanges();
                return RedirectToAction("ViewAssignments/" + surveyId);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void SendMailToEmployees(long[] userIds)
        {
            var emails = _userRepo.GetEmailForUsers(userIds);

            // Replace them with @Resources and maybe add html to body.
            var subject = "New interview assignment";
            var body = "You have been assigned a new interview. Please log in to HRRadar to complete it.";

            foreach (var email in emails)
            {
                if (!string.IsNullOrEmpty(email))
                {
                    mailer.SendMail(email, subject, body);
                }                
            }
        }

        private async Task AddNotificationsForEmployeesAsync(long[] userIds, long surveyId)
        {
            var messages = new List<Message>();
            var messageBody = string.Empty;
            var survey = await _surveyRepo.GetSurveyBySurveyId(surveyId);

            if (survey.EndDate != null)
            {
                messageBody = String.Format("{0} has been assigned to you. You must complete it until {1}", survey.SurveyTitle, ((DateTime)survey.EndDate).ToShortDateString());
            }
            else
            {
                messageBody = String.Format("{0} has been assigned to you.", survey.SurveyTitle);
            }
            

            foreach (var userId in userIds)
            {
                var message = new Message()
                {
                    FromUserId = GetUserByAspNetUserId().UserId,
                    ToUserId = userId,
                    MessageType = Enums.MessageType.NOTIFICATION,
                    Subject = "New interview assignment",
                    MessageBody = messageBody,
                    RelatedId = surveyId,
                    DateSent = DateTime.Now,
                    IsActive = true
                };

                messages.Add(message);
            }

            await _userRepo.AddNotificationsAsync(messages);
        }
    }
}
