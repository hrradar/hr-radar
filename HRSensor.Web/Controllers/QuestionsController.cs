﻿using HRSensor.Common;
using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HRSensor.Web.Controllers
{
    [Authorize(Roles = Constants.ARCHITECT_ROLE + ", " + Constants.HRMONITOR_ROLE)]
    public class QuestionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: questions
        public ActionResult Index(int? cmbQuestionnaire, int? cmbQuestionSector)
        {

            List<Questionnaire> objQlist = (from data in db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true)
                                    select data).ToList();
            Questionnaire objQ = new Questionnaire();
            objQ.QuestionnaireTitle = "-- Select Questionnaire --";
            objQ.QuestionnaireId = 0;
            objQlist.Insert(0, objQ);
            SelectList objQdata = new SelectList(objQlist, "QuestionnaireId", "QuestionnaireTitle", 0);
            ViewBag.Questionnaires = objQdata;

            List<QuestionSector> objQslist = (from data in db.QuestionSectors.OrderBy(x => x.QuestionSectorName).Where(x => x.IsActive == true)
                                            select data).ToList();
            QuestionSector objQs = new QuestionSector();
            objQs.QuestionSectorName = "-- Select Question Sector --";
            objQs.QuestionSectorId = 0;
            objQslist.Insert(0, objQs);
            SelectList objQsdata = new SelectList(objQslist, "QuestionSectorId", "QuestionSectorName", 0);
            ViewBag.QuestionSectors = objQsdata;

            var questions = db.Questions.Where(c => c.IsActive == true);

            if (cmbQuestionnaire != 0 && cmbQuestionnaire != null)
            {
                questions = questions.Where(c => c.QuestionnaireId == cmbQuestionnaire);
            }

            if (cmbQuestionSector != 0 && cmbQuestionSector != null)
            {
                questions = questions.Where(c => c.QuestionSectorId == cmbQuestionSector);
            }

            ModelState.Clear();
            return View(questions.ToList());
        }

        // GET: questions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }

            return View(question);
        }

        //GET: questions/Create
        public ActionResult Create()
        {
            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle");
            ViewBag.QuestionSectors = new SelectList(db.QuestionSectors.OrderBy(x => x.QuestionSectorName).Where(x => x.IsActive == true), "QuestionSectorId", "QuestionSectorName");
            if (HttpContext.Request.IsAjaxRequest())
                return Json(ViewBag, JsonRequestBehavior.AllowGet);
            return View();
        }

        //POST: questions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuestionId, QuestionTitle, QuestionCode, QuestionnaireId, QuestionSectorId, Lang, Type, DifficultyLevel, Weight, HasFeedback, HasHint, IsActive")] Question question)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (question.QuestionnaireId != 0)
                    {
                        var qName = db.Questionnaires.Find(question.QuestionnaireId);
                        question.Questionnaire = qName;
                    }

                    if (question.QuestionSectorId != 0)
                    {
                        var qsName = db.QuestionSectors.Find(question.QuestionSectorId);
                        question.QuestionSector = qsName;
                    }

                    question.IsActive = true;

                    db.Questions.Add(question);

                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            catch (Exception exception)
            {
                var ex = exception.Message;
            }

            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", question.QuestionnaireId);
            ViewBag.QuestionSectors = new SelectList(db.QuestionSectors.OrderBy(x => x.QuestionSectorName).Where(x => x.IsActive == true), "QuestionSectorId", "QuestionSectorName", question.QuestionSectorId);

            return View(question);
        }


        // GET: questions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }

            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", question.QuestionnaireId);
            ViewBag.QuestionSectors = new SelectList(db.QuestionSectors.OrderBy(x => x.QuestionSectorName).Where(x => x.IsActive == true), "QuestionSectorId", "QuestionSectorName", question.QuestionSectorId);

            return View(question);
        }

        // POST: questions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuestionId, QuestionTitle, QuestionCode, QuestionnaireId, QuestionSectorId, Lang, Type, DifficultyLevel, Weight, HasFeedback, HasHint, IsActive")] Question question)
        {
            if (ModelState.IsValid)
            {
                var quest = db.Questions.Find(question.QuestionId);

                quest.QuestionTitle = question.QuestionTitle;
                quest.QuestionCode = question.QuestionCode;
                quest.QuestionnaireId = question.QuestionnaireId;
                quest.QuestionSectorId = question.QuestionSectorId;
                quest.Lang = question.Lang;
                quest.Type = question.Type;
                quest.DifficultyLevel = question.DifficultyLevel;
                quest.Weight = question.Weight;
                quest.HasFeedback = question.HasFeedback;
                quest.HasHint = question.HasHint;

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", question.QuestionnaireId);
            ViewBag.QuestionSectors = new SelectList(db.QuestionSectors.OrderBy(x => x.QuestionSectorName).Where(x => x.IsActive == true), "QuestionSectorId", "QuestionSectorName", question.QuestionSectorId);

            return View(question);
        }

        // GET: questions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }

            ViewBag.Questionnaires = new SelectList(db.Questionnaires.OrderBy(x => x.QuestionnaireTitle).Where(x => x.IsActive == true), "QuestionnaireId", "QuestionnaireTitle", question.QuestionnaireId);
            ViewBag.QuestionSectors = new SelectList(db.QuestionSectors.OrderBy(x => x.QuestionSectorName).Where(x => x.IsActive == true), "QuestionSectorId", "QuestionSectorName", question.QuestionSectorId);

            return View(question);
        }

        // POST: questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = db.Questions.Find(id);
            question.IsActive = false;
            db.Entry(question).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}