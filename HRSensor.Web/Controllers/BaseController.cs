﻿using HRSensor.Models;
using HRSensor.Repositories.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Web.Mvc;

namespace HRSensor.Web.Controllers
{
    public class BaseController : Controller
    {
        protected string aspNetUserId = System.Web.HttpContext.Current.User.Identity.GetUserId();
        private readonly IUserRepo _userRepo;

        public BaseController() { }

        public BaseController(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }

        protected User GetUserByAspNetUserId()
        {
            return _userRepo.GetUserByAspNetUserId(aspNetUserId);
        }
    }
}