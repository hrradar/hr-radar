﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class MarkPoint
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public float Value { get; set; }
        public float XAxis { get; set; }
        public float YAxis { get; set; }
        public float SymbolSize { get; set; }

        public MarkPoint(string type, string name)
        {
            this.Type = type;
            this.Name = name;
        }

        public MarkPoint(string name, float value, float xAxis, float yAxis)
        {
            this.Name = name;
            this.Value = value;
            this.XAxis = xAxis;
            this.YAxis = yAxis;
        }

        public MarkPoint(string name, float value, float xAxis, float yAxis, float symbolSize)
        {
            this.Name = name;
            this.Value = value;
            this.XAxis = xAxis;
            this.YAxis = yAxis;
            this.SymbolSize = symbolSize;
        }
    }
}