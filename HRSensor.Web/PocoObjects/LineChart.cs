﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class LineChart : BaseChart
    {
        public List<float> SeriesData { get; set; }
        public List<string> XAxisData { get; set; }

        public LineChart(List<float> seriesData, List<string> xAxisData, List<string> legendData)
        {
            this.SeriesData = seriesData;
            this.XAxisData = xAxisData;
            this.LegendData = legendData;
        }
    }
}