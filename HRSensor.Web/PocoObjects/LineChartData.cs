﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class LineChartData
    {
        public List<float> SeriesData { get; set; }
        public List<string> XAxisData { get; set; }
    }
}