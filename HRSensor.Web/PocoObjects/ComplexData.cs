﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class ComplexData
    {
        public float Value { get; set; }
        public string Name { get; set; }

        public ComplexData(float value, string name)
        {
            this.Value = value;
            this.Name = name;
        }
    }
}