﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class CompanySectorAnswerTitled : CompanySectorAnswer
    {
        public string Title { get; set; }
    }
}