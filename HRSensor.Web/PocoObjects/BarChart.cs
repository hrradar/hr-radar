﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class BarChart :BaseChart
    {
        public List<List<float>> SeriesData { get; set; }
        public List<string> XAxisData { get; set; }
        public List<List<MarkPoint>> MarkPoints { get; set; }

        public BarChart(List<List<float>> seriesData, List<string> xAxisData, List<string> legendData)
        {
            this.SeriesData = seriesData;
            this.LegendData = legendData;
            this.XAxisData = xAxisData;
        }
    }
}