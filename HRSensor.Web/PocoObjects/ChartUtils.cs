﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class ChartUtils
    {
        private static ChartUtils instance = new ChartUtils();
        private static Random random = new Random();

        public static ChartUtils getInstance()
        {
            return instance;
        }

        public BaseChart getChart(string name)
        {
            BaseChart chart = null;
            switch (name)
            {
                case "cprs":
                    chart = GetCprs();
                    break;
                case "cprs_history":
                    chart = GetCprsHistory();
                    break;
                case "job_satisfaction":
                    chart = GetJobSatisfactionChart();
                    break;
                case "job_satisfaction_history":
                    chart = GetJobSatisfactionHistoryChart();
                    break;
                case "health_wellbeing":
                    chart = GetHealthWellbeingChart();
                    break;
            }

            return chart;
        }

        private List<float> GetRandomData(int maxValue, int numOfData)
        {
            List<float> data = new List<float>();
            for (int i = 0; i < numOfData; i++)
            {
                data.Add(random.Next(maxValue));
            }

            return data;
        }

        private MeterChart GetCprs()
        {
            List<ComplexData> seriesData = new List<ComplexData>();
            List<string> legendData = new List<string>();
            seriesData.Add(new ComplexData(0, "CHRI"));
            legendData.Add("CHRffffI");

            return new MeterChart(seriesData, legendData);
        }

        private LineChart GetCprsHistory()
        {
            List<string> legendData = new List<string>();
            legendData.Add("CHRI History");
            List<string> xAxisData = new List<string>()
            {
                "12/2017", "1/2018", "2/2018", "3/2018"
            };

            return new LineChart(new List<float>() { 0,0,0,0}, xAxisData, legendData);
        }

        public static MeterChart GetMeterChart(CompanySectorAnswer companySectorAnswer)
        {
            List<ComplexData> seriesData = new List<ComplexData>();
            List<string> legendData = new List<string>();
            
            seriesData.Add(new ComplexData(GetCompanySectorAnswerValue(companySectorAnswer), "Current Value"));
            legendData.Add("test");

            return new MeterChart(seriesData, legendData);
        }

        public static LineChart GetLineChart(LineChartData lineChartData)
        {
            List<string> legendData = new List<string>() { "CHRI History" };
         
            return new LineChart(lineChartData.SeriesData, lineChartData.XAxisData, legendData);
        }

        public static MeterChart GetCprsChart(float data)
        {
            List<ComplexData> seriesData = new List<ComplexData>();
            List<string> legendData = new List<string>();
            seriesData.Add(new ComplexData(data, "CHRI"));
            legendData.Add("CHRffffI");

            return new MeterChart(seriesData, legendData);
        }

        public static LineChart GetCprsHistoryChart(LineChartData lineChartData)
        {
            List<string> legendData = new List<string>();
            legendData.Add("CHRI History");

            return new LineChart(lineChartData.SeriesData, lineChartData.XAxisData, legendData);
        }

        public PieChart GetJobSatisfactionChart()
        {
            List<ComplexData> seriesData = new List<ComplexData>();
            List<string> legendData = new List<string>()
            {
                "Yes", "No"
            };

            seriesData.Add(new ComplexData(GetRandomData(500, 1)[0], "Yes"));
            seriesData.Add(new ComplexData(GetRandomData(500, 1)[0], "No"));

            return new PieChart(seriesData, legendData);
        }

        private LineChart GetJobSatisfactionHistoryChart()
        {
            List<string> legendData = new List<string>();
            legendData.Add("Job Satisfaction History");
            List<string> xAxisData = new List<string>()
            {
                "12/2017", "1/2018", "2/2018", "3/2018"
            };

            return new LineChart(GetRandomData(5, 5), xAxisData, legendData);
        }

        private BarChart GetHealthWellbeingChart()
        {
            List<string> legendData = new List<string>();
            legendData.Add("Job Satisfaction History");
            List<string> xAxisData = new List<string>()
            {
                "12/2017", "1/2018", "2/2018", "3/2018"
            };
            List<List<float>> seriesData = new List<List<float>>();
            seriesData.Add(GetRandomData(5, 5));
            seriesData.Add(GetRandomData(5, 5));

            BarChart healthWellbeingChart = new BarChart(seriesData, xAxisData, legendData);
            healthWellbeingChart.MarkPoints = new List<List<MarkPoint>>();

            List<MarkPoint> markPointsOne = new List<MarkPoint>();
            markPointsOne.Add(new MarkPoint("max", "Max"));
            markPointsOne.Add(new MarkPoint("min", "Min"));

            List<MarkPoint> markPointsTwo = new List<MarkPoint>();
            markPointsTwo.Add(new MarkPoint("The highest year", GetRandomData(200, 1)[0], 7, GetRandomData(200, 1)[0], 18));
            markPointsTwo.Add(new MarkPoint("Year minimum", GetRandomData(5, 1)[0], 11, 3));


            healthWellbeingChart.MarkPoints.Add(markPointsOne);
            healthWellbeingChart.MarkPoints.Add(markPointsTwo);

            return healthWellbeingChart;
        }

        private static float GetCompanySectorAnswerValue(CompanySectorAnswer companySectorAnswer)
        {
            return companySectorAnswer == null ? 0 : GetPctRounded(companySectorAnswer.MathAvg);
        }

        private static float GetPctRounded(float value)
        {
            return (float)Math.Round(value * 100, 2);
        }
    }
}