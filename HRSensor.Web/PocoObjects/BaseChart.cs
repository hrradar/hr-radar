﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSensor.Web.PocoObjects
{
    public abstract class BaseChart
    {
        public List<string> LegendData { get; set; }        
    }
}
