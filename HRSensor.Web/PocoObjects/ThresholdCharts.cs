﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class ThresholdCharts
    {
        public string QuestionSectorTitle { get; set; }
        public long QuestionSectorId { get; set; }
        public MeterChart MeterChart { get; set; }
        public float ProgressBar { get; set; }
        public LineChart LineChart { get; set; }
    }
}