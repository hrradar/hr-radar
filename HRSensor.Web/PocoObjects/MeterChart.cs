﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRSensor.Web.PocoObjects
{
    public class MeterChart : BaseChart 
    {
        public List<ComplexData> SeriesData { get; set; }

        public MeterChart(List<ComplexData> seriesData, List<string> legendData)
        {
            this.SeriesData = seriesData;
            this.LegendData = legendData;
        }

    }
}