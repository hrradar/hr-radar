﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HRSensor.Common.Enums;

namespace HRSensor.Repositories.Interfaces
{
    public interface ISurveyRepo
    {
        List<Survey> GetSurveysByUserId(long userId);
        List<UserSurvey> GetUserSurveysByUserId(long userId);
        List<Question> GetSurveyQuestionsBySurveyId(long surveyId, Lang language);
        Task<Survey> GetSurveyBySurveyId(long surveyId);
    }
}
