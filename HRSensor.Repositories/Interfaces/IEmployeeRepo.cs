﻿using HRSensor.Models;

namespace HRSensor.Repositories.Interfaces
{
    public interface IEmployeeRepo
    {
        bool AddMessage(Message message);
    }
}
