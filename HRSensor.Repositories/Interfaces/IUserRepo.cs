﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSensor.Repositories.Interfaces
{
    public interface IUserRepo
    {
        User GetUserByAspNetUserId(string aspNetUserId);
        User GetUserByUserId(int userId);
        ApplicationUser GetApplicationUserByAspNetUserId(string aspNetUserId);
        User UpdateUser(User user);
        bool DeleteUser(int userId);
        List<User> GetUsersForAssignment(long surveyId, long organizationId);
        List<User> GetAuthors(long? organizationId);
        List<string> GetEmailForUsers(long[] userIds);
        Task AddNotificationsAsync(List<Message> messages);
        List<Message> GetMessagesByUserId(long userId);
    }
}
