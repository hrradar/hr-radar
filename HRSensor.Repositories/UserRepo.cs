﻿using HRSensor.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRSensor.Models;
using System.Data.Entity;
using System.Data.SqlClient;

namespace HRSensor.Repositories
{
    public class UserRepo : IUserRepo
    {
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();

        private const string GET_USERS_FOR_ASSIGNMENT = @"select u.* from users u 
                                                            inner join AspNetUserRoles anur
                                                                on u.AspNetUserId=anur.UserId
                                                            inner join AspNetRoles anr
                                                                on anur.RoleId=anr.Id
                                                            where u.OrganizationId=@OrganizationId
                                                                and not exists (select 1 from UserSurveys us where us.SurveyId=@SurveyId and us.UserId=u.UserId) 
                                                                and anr.Name='Employee' and u.IsActive=1 
                                                            order by u.UserId";

        private const string GET_AUTHORS = @"select u.* from users u
                                                inner join AspNetUserRoles anur
                                                    on u.AspNetUserId= anur.UserId
                                                inner join AspNetRoles anr
                                                    on anur.RoleId= anr.Id
                                                where u.OrganizationId= @OrganizationId
                                                    and anr.Name= 'Architect' and u.IsActive= 1
                                                order by u.UserId";

        public bool DeleteUser(int userId)
        {
            var user = _dbContext.User.Find(userId);
            if (user == null)
            {
                return false;
            }

            var aspNetUser = _dbContext.Users.Where(u => u.Id == user.AspNetUserId).FirstOrDefault();

            user.IsActive = false;
            user.AspNetUserId = "999999999999999"; // To track deleted users
            _dbContext.Users.Remove(aspNetUser);            
            
            return (_dbContext.SaveChanges() > 0 ? true : false); 
        }

        public ApplicationUser GetApplicationUserByAspNetUserId(string aspNetUserId)
        {
            var user = _dbContext.Users.Where(u => u.Id == aspNetUserId).SingleOrDefault();
            return user;
        }

        //public UserRepo(ApplicationDbContext dbContext)
        //{
        //    _dbContext = dbContext;
        //}

        public User GetUserByAspNetUserId(string aspNetUserId)
        {
            var user = _dbContext.User.Where(u => u.AspNetUserId == aspNetUserId).FirstOrDefault();
            return user;
        }

        public User GetUserByUserId(int userId)
        {
            var user = _dbContext.User.Include("ContactDetails").Where(u => u.UserId == userId).FirstOrDefault();
            return user;
        }

        public User UpdateUser(User user)
        {
            var dbMember = _dbContext.User.Where(u => u.UserId == user.UserId).Include(u => u.ContactDetails).SingleOrDefault();
            if (dbMember == null) return null;

            //if (dbMember.ContactDetails == null)
            //    _dbContext.Entry(dbMember.ContactDetails).State = EntityState.Added;
            //else
            //    _dbContext.Entry(dbMember.ContactDetails).State = EntityState.Modified;

            dbMember.FirstName = user.FirstName;
            dbMember.LastName = user.LastName;
            dbMember.BirthDate = user.BirthDate;
            dbMember.Gender = user.Gender;
            dbMember.ContactDetails = user.ContactDetails;

            _dbContext.SaveChanges();

            return dbMember;
        }

        public List<User> GetUsersForAssignment(long surveyId, long organizationId)
        {
            var users = _dbContext.User.SqlQuery(GET_USERS_FOR_ASSIGNMENT, new SqlParameter("SurveyId", surveyId), new SqlParameter("OrganizationId", organizationId)).ToList();

            return users;
        }
        public List<User> GetAuthors(long? organizationId)
        {
            var users = _dbContext.User.SqlQuery(GET_AUTHORS, new SqlParameter("OrganizationId", organizationId)).ToList();

            return users;
        }

        public List<string> GetEmailForUsers(long[] userIds)
        {
            var emails = _dbContext.UserContactDetails.Where(cd => userIds.Contains(cd.UserContactDetailId))
                .Select(cd => cd.Email).ToList();            

            return emails;
        }

        public async Task AddNotificationsAsync(List<Message> messages)
        {
            _dbContext.Messages.AddRange(messages);
            await _dbContext.SaveChangesAsync();
        }

        public List<Message> GetMessagesByUserId(long userId)
        {
            var messages = _dbContext.Messages.Where(m => m.ToUserId == userId && m.MessageType == Common.Enums.MessageType.NOTIFICATION && m.IsActive == true)
                .OrderByDescending(m => m.DateSent).Take(5).ToList();

            return messages;
        }
    }
}
