﻿using HRSensor.Repositories.Interfaces;
using System;
using HRSensor.Models;

namespace HRSensor.Repositories
{
    public class EmployeeRepo : IEmployeeRepo
    {
        private readonly ApplicationDbContext _dbContext;

        public EmployeeRepo(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool AddMessage(Message message)
        {
            _dbContext.Messages.Add(message);
            return _dbContext.SaveChanges() > 0 ? true : false;
        }
    }
}
