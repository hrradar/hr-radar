﻿using HRSensor.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using HRSensor.Models;
using System.Data.SqlClient;
using System.Data.Entity;
using static HRSensor.Common.Enums;
using System.Threading.Tasks;

namespace HRSensor.Repositories
{
    public class SurveyRepo : ISurveyRepo
    {
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();

        private const string GET_SURVEYS_BY_USERID = @"SELECT S.* FROM Surveys S
                                                        INNER JOIN UserSurveys US on US.SurveyId = S.SurveyId
                                                            AND US.UserId = @UserId  
                                                        WHERE S.IsActive = 1 AND S.StartDate <= GETDATE() 
                                                            AND (S.EndDate >= GETDATE() OR S.EndDate IS NULL)";

        //public SurveyRepo(ApplicationDbContext dbContext)
        //{
        //    _dbContext = dbContext;
        //}

        public List<Survey> GetSurveysByUserId(long userId)
        {
            var surveys = _dbContext.Surveys.SqlQuery(GET_SURVEYS_BY_USERID, new SqlParameter("UserId", userId)).ToList();

            return surveys;
        }

        public List<Question> GetSurveyQuestionsBySurveyId(long surveyId, Lang language)
        {
            var survey = _dbContext.Surveys.SingleOrDefault(s => s.SurveyId == surveyId);

            if (survey == null) return null;

            var questions = _dbContext.Questions.Where(q => q.Lang == language)
                .Include(q => q.QuestionAnswers)                
                .Include(q => q.QuestionSector)
                .Where(q => q.QuestionnaireId == survey.QuestionnaireId && q.IsActive == true && q.QuestionSector.IsActive == true)
                .OrderBy(q => q.QuestionCode).ToList();
            
            return questions;
        }

        public List<UserSurvey> GetUserSurveysByUserId(long userId)
        {
            var userSurveys = _dbContext.UserSurveys.Where(us => us.UserId == userId).Include(us => us.Survey)
                .Where(us => us.Survey.StartDate <= DateTime.Now && (us.Survey.EndDate >= DateTime.Now || us.Survey.EndDate == null )
                    && us.Survey.IsActive == true).AsNoTracking().ToList();

            return userSurveys;
        }

        public async Task<Survey> GetSurveyBySurveyId(long surveyId)
        {
            var survey = await _dbContext.Surveys.Where(s => s.SurveyId == surveyId).SingleOrDefaultAsync();
            return survey;
        }
    }
}
