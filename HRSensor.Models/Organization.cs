﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRSensor.Models
{
    public class Organization
    {
        public long OrganizationId { get; set; }
        [MaxLength(500)]
        [Display(Name = "OrganizationName", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string OrganizationName { get; set; }
        public bool IsActive { get; set; } = true;

        public ICollection<Survey> Surveys { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<BusinessUnit> BusinessUnits { get; set; }
    }
}