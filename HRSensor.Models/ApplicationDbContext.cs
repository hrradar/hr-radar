﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HRSensor.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<BusinessUnit> BusinessUnits { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<GeoLocation> GeoLocations { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionAnswer> QuestionAnswers { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<QuestionSector> QuestionSectors { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserAnswer> UserAnswers { get; set; }
        public DbSet<UserContactDetail> UserContactDetails { get; set; }
        public DbSet<UserSurvey> UserSurveys { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<UserSectorAnswer> UserSectorAnswers { get; set; }
        public DbSet<CompanySectorAnswer> CompanySectorAnswers { get; set; }

        public ApplicationDbContext() : base("HRSensorEntities", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }


    //public class ApplicationDbContext : DbContext
    //{
    //    public DbSet<BusinessUnit> BusinessUnits { get; set; }
    //    public DbSet<Department> Departments { get; set; }
    //    public DbSet<GeoLocation> GeoLocations { get; set; }
    //    public DbSet<Message> Messages { get; set; }
    //    public DbSet<Question> Questions { get; set; }
    //    public DbSet<QuestionAnswer> QuestionAnswers { get; set; }
    //    public DbSet<Questionnaire> Questionnaires { get; set; }
    //    public DbSet<QuestionSector> QuestionSectors { get; set; }
    //    public DbSet<Survey> Surveys { get; set; }
    //    public DbSet<User> User { get; set; }
    //    public DbSet<UserAnswer> UserAnswers { get; set; }
    //    public DbSet<UserContactDetail> UserContactDetails { get; set; }
    //    public DbSet<UserSurvey> UserSurveys { get; set; }

    //    public ApplicationDbContext()
    //        : base("HRSensorEntities")
    //    {
    //    }

    //    public static ApplicationDbContext Create()
    //    {
    //        return new ApplicationDbContext();
    //    }


    //}
}
