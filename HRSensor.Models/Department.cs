﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRSensor.Models
{
    public class Department
    {
        public long DepartmentId { get; set; }
        [MaxLength(500)]
        [Display(Name = "DepartmentName", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string DepartmentName { get; set; }
        public long BusinessUnitId { get; set; }
        public long? GeoLocationId { get; set; }
        public bool IsActive { get; set; } = true;

        public BusinessUnit BusinessUnit { get; set; }
        public GeoLocation GeoLocation { get; set; }

    }
}