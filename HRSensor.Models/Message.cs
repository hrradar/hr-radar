﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class Message
    {
        public long MessageId { get; set; }
        public long FromUserId { get; set; }
        public long ToUserId { get; set; }
        public MessageType MessageType { get; set; }
        [MaxLength(100)]
        public string Subject { get; set; }
        public string MessageBody { get; set; }
        public long? RelatedId { get; set; }
        public DateTime DateSent { get; set; }
        public bool IsActive { get; set; } = true;

    }
}