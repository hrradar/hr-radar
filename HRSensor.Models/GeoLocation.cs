﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRSensor.Models
{
    public class GeoLocation
    {
        public long GeoLocationId { get; set; }
        [MaxLength(500)]
        [Display(Name = "GeoLocationName", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string GeoLocationName { get; set; }
        public bool IsActive { get; set; } = true;

        public ICollection<Department> Departments { get; set; }
        public ICollection<User> Users { get; set; }
    }
}