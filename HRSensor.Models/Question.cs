﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class Question
    {
        public long QuestionId { get; set; }

        [Display(Name = "QuestionTitle", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string QuestionTitle { get; set; }

        [Display(Name = "QuestionCode", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string QuestionCode { get; set; }
        public long QuestionnaireId { get; set; }

        [Display(Name = "QuestionSectorId", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public long? QuestionSectorId { get; set; }
        public Lang Lang { get; set; }
        public QuestionType Type { get; set; }
        public DifficultyLevel DifficultyLevel { get; set; }
        public Weight Weight { get; set; }

        [Display(Name = "HasFeedback", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public bool HasFeedback { get; set; }
        public bool HasHint { get; set; }
        public bool IsActive { get; set; } = true;

        public QuestionSector QuestionSector { get; set; }
        public Questionnaire Questionnaire { get; set; }
        public ICollection<QuestionAnswer> QuestionAnswers { get; set; }

    }
}