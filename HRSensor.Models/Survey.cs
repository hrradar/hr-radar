﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class Survey
    {
        public long SurveyId { get; set; }
        [MaxLength(500)]

        [Required(ErrorMessage = "Title is required.")]
        [Display(Name = "SurveyTitle", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string SurveyTitle { get; set; }

        [Required(ErrorMessage = "Code is required.")]
        [Display(Name = "SurveyCode", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string SurveyCode { get; set; }

        [Required(ErrorMessage = "Organization is required.")]
        [Display(Name = "OrganizationId", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public long OrganizationId { get; set; }

        [Display(Name = "BusinessUnitId", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public long? BusinessUnitId { get; set; }

        [Required(ErrorMessage = "Questionnaire is required.")]
        [Display(Name = "QuestionnaireId", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public long QuestionnaireId { get; set; }

        [Display(Name = "DepartmentId", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public long? DepartmentId { get; set; }

        [Display(Name = "GeoLocationId", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public long? GeoLocationId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Required(ErrorMessage = "Start Date is required.")]
        [Display(Name = "StartDate", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public DateTime? StartDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Required(ErrorMessage = "End Date is required.")]
        [Display(Name = "EndDate", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public DateTime? EndDate { get; set; }

        public SurveyStatus Status { get; set; }
        public long? UserId { get; set; }


        [DisplayFormat(DataFormatString = "{0:d}")]
        [Display(Name = "CreationDate", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public DateTime CreationDate { get; set; } = DateTime.Now;

        [Display(Name = "CPRS", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public int? CPRS { get; set; }

        [Display(Name = "IsActive", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public bool IsActive { get; set; } = true;

        [Required(ErrorMessage = "Time Needed is required.")]
        [Display(Name = "TimeNeeded", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public int TimeNeeded { get; set; }

        [Display(Name = "HasHint", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public bool HasHint { get; set; }

        [Display(Name = "WithReview", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public bool WithReview { get; set; }

        [Display(Name = "MandatoryAnswersToAllQuestions", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public bool MandatoryAnswersToAllQuestions { get; set; }

        [Required(ErrorMessage = "No Of Questions Per Page is required.")]
        [Display(Name = "NoOfQuestionsPerPage", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public int NoOfQuestionsPerPage { get; set; }

        [Display(Name = "WithResultPage", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public bool WithResultPage { get; set; }

        [Display(Name = "CalculateDistinctWeighting", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public bool CalculateDistinctWeighting { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual Questionnaire Questionnaire { get; set; }
        public virtual BusinessUnit BusinessUnit { get; set; }
        public virtual Department Department { get; set; }
        public virtual GeoLocation GeoLocation { get; set; }
        public virtual User Author { get; set; }
    }
}