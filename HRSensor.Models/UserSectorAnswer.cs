﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class UserSectorAnswer
    {
        public long UserSectorAnswerId { get; set; }
        public long QuestionSectorId { get; set; }
        public long? UserId { get; set; }
        public long SurveyId { get; set; }
        public float MathAvg { get; set; }
    }
}