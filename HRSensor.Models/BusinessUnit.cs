﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRSensor.Models
{
    public class BusinessUnit
    {
        public long BusinessUnitId { get; set; }
        [MaxLength(500)]
        [Display(Name = "BusinessUnitName", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string BusinessUnitName { get; set; }
        public long OrganizationId { get; set; }
        public bool IsActive { get; set; } = true;

        public ICollection<Department> Departments { get; set; }
        public ICollection<Survey> Surveys { get; set; }
        public ICollection<User> Users { get; set; }
    }
}