﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRSensor.Models
{
    public class Questionnaire
    {
        public long QuestionnaireId { get; set; }

        [Display(Name = "QuestionnaireTitle", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string QuestionnaireTitle { get; set; }
        public string Description { get; set; }
        public int TimeNeeded { get; set; }
        public bool HasHint { get; set; }
        public bool WithReview { get; set; }
        public bool MandatoryAnswersToAllQuestions { get; set; }
        public int NoOfQuestionsPerPage { get; set; }
        public bool WithResultPage { get; set; }
        public bool CalculateDistinctWeighting { get; set; }
        public bool IsActive { get; set; } = true;

        public ICollection<Survey> Surveys { get; set; }
    }
}