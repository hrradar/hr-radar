﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class UserSurvey
    {
        public long UserSurveyId { get; set; }
        public long UserId { get; set; }
        public long SurveyId { get; set; }
        public SurveyStatus Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; } = true;

        public Survey Survey { get; set; }
        public User User { get; set; }
    }
}