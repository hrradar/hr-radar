﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class UserAnswer
    {
        public long UserAnswerId { get; set; }
        public long QuestionId { get; set; }
        public long? UserId { get; set; }
        public long SurveyId { get; set; }
        public QuestionType Type { get; set; }
        public string FreeText { get; set; }
        public Weight? MultipleChoiceValue { get; set; }

        public Question Question { get; set; }
        //public Survey Survey { get; set; }
    }
}