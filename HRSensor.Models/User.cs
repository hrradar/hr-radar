﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class User
    {
        public long UserId { get; set; }
        public string AspNetUserId { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        [Display(Name = "FullName", ResourceType = typeof(HRSensor.Resources.Properties.Resources_en))]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string FatherName { get; set; }
        public Gender Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string HRCode { get; set; }
        public long? OrganizationId { get; set; }
        public long? DepartmentId { get; set; }
        public long? BusinessUnitId { get; set; }
        public long? GeoLocationId { get; set; }
        public bool HasAcceptedTerms { get; set; }
        public bool IsActive { get; set; } = true;

        public BusinessUnit BusinessUnit { get; set; }
        public GeoLocation GeoLocation { get; set; }
        public Organization Organization { get; set; }
        public Department Department { get; set; }
        public UserContactDetail ContactDetails { get; set; }
    }
}