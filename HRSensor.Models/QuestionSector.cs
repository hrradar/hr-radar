﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRSensor.Models
{
    public class QuestionSector
    {
        public long QuestionSectorId { get; set; }
        public string Code { get; set; }
        [MaxLength(500)]
        public string QuestionSectorName { get; set; }
        public bool IsActive { get; set; } = true;
        public string QuestionSectorHeader { get; set; }
        public bool ParticipatesInCalculation { get; set; } 

        public IEnumerable<Question> Questions { get; set; }
    }
}