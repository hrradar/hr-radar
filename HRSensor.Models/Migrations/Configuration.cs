namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HRSensor.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HRSensor.Models.ApplicationDbContext context)
        {
            context.Roles.AddOrUpdate(
                r => r.Name,
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "Architect" },
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "HRMonitor" },
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = "Employee" }
                );
        }
    }
}
