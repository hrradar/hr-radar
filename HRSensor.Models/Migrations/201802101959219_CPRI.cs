namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CPRI : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanySectorAnswers", "CPRI", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanySectorAnswers", "CPRI");
        }
    }
}
