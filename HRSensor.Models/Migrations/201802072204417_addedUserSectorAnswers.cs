namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedUserSectorAnswers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserSectorAnswers",
                c => new
                    {
                        UserSectorAnswerId = c.Long(nullable: false, identity: true),
                        QuestionSectorId = c.Long(nullable: false),
                        UserId = c.Long(),
                        SurveyId = c.Long(nullable: false),
                        MathAvg = c.Single(nullable: false),
                        QuestionSector_QuestionId = c.Long(),
                    })
                .PrimaryKey(t => t.UserSectorAnswerId)
                .ForeignKey("dbo.Questions", t => t.QuestionSector_QuestionId)
                .Index(t => t.QuestionSector_QuestionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserSectorAnswers", "QuestionSector_QuestionId", "dbo.Questions");
            DropIndex("dbo.UserSectorAnswers", new[] { "QuestionSector_QuestionId" });
            DropTable("dbo.UserSectorAnswers");
        }
    }
}
