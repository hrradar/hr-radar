namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanySectorAnswers",
                c => new
                    {
                        CompanySectorAnswerId = c.Long(nullable: false, identity: true),
                        QuestionSectorId = c.Long(nullable: false),
                        OrganizationId = c.Long(nullable: false),
                        SurveyId = c.Long(nullable: false),
                        MathAvg = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.CompanySectorAnswerId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CompanySectorAnswers");
        }
    }
}
