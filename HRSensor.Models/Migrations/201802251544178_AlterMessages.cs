namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterMessages : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "RelatedId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "RelatedId");
        }
    }
}
