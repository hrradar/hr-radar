namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserSectorAnswers", "QuestionSector_QuestionId", "dbo.Questions");
            DropIndex("dbo.UserSectorAnswers", new[] { "QuestionSector_QuestionId" });
            DropColumn("dbo.UserSectorAnswers", "QuestionSector_QuestionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserSectorAnswers", "QuestionSector_QuestionId", c => c.Long());
            CreateIndex("dbo.UserSectorAnswers", "QuestionSector_QuestionId");
            AddForeignKey("dbo.UserSectorAnswers", "QuestionSector_QuestionId", "dbo.Questions", "QuestionId");
        }
    }
}
