namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BusinessUnits",
                c => new
                    {
                        BusinessUnitId = c.Long(nullable: false, identity: true),
                        BusinessUnitName = c.String(maxLength: 500),
                        OrganizationId = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.BusinessUnitId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .Index(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentId = c.Long(nullable: false, identity: true),
                        DepartmentName = c.String(maxLength: 500),
                        BusinessUnitId = c.Long(nullable: false),
                        GeoLocationId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentId)
                .ForeignKey("dbo.BusinessUnits", t => t.BusinessUnitId, cascadeDelete: true)
                .ForeignKey("dbo.GeoLocations", t => t.GeoLocationId)
                .Index(t => t.BusinessUnitId)
                .Index(t => t.GeoLocationId);
            
            CreateTable(
                "dbo.GeoLocations",
                c => new
                    {
                        GeoLocationId = c.Long(nullable: false, identity: true),
                        GeoLocationName = c.String(maxLength: 500),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.GeoLocationId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        AspNetUserId = c.String(),
                        LastName = c.String(maxLength: 50),
                        FirstName = c.String(maxLength: 50),
                        FullName = c.String(maxLength: 100),
                        FatherName = c.String(maxLength: 50),
                        Gender = c.Int(nullable: false),
                        BirthDate = c.DateTime(),
                        HRCode = c.String(),
                        OrganizationId = c.Long(),
                        DepartmentId = c.Long(),
                        BusinessUnitId = c.Long(),
                        GeoLocationId = c.Long(),
                        HasAcceptedTerms = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.BusinessUnits", t => t.BusinessUnitId)
                .ForeignKey("dbo.Departments", t => t.DepartmentId)
                .ForeignKey("dbo.GeoLocations", t => t.GeoLocationId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId)
                .Index(t => t.OrganizationId)
                .Index(t => t.DepartmentId)
                .Index(t => t.BusinessUnitId)
                .Index(t => t.GeoLocationId);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        OrganizationId = c.Long(nullable: false, identity: true),
                        OrganizationName = c.String(maxLength: 500),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Surveys",
                c => new
                    {
                        SurveyId = c.Long(nullable: false, identity: true),
                        SurveyTitle = c.String(nullable: false, maxLength: 500),
                        SurveyCode = c.String(nullable: false),
                        OrganizationId = c.Long(nullable: false),
                        BusinessUnitId = c.Long(),
                        QuestionnaireId = c.Long(nullable: false),
                        DepartmentId = c.Long(),
                        GeoLocationId = c.Long(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        UserId = c.Long(),
                        CreationDate = c.DateTime(nullable: false),
                        CPRS = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                        TimeNeeded = c.Int(nullable: false),
                        HasHint = c.Boolean(nullable: false),
                        WithReview = c.Boolean(nullable: false),
                        MandatoryAnswersToAllQuestions = c.Boolean(nullable: false),
                        NoOfQuestionsPerPage = c.Int(nullable: false),
                        WithResultPage = c.Boolean(nullable: false),
                        CalculateDistinctWeighting = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SurveyId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.BusinessUnits", t => t.BusinessUnitId)
                .ForeignKey("dbo.Departments", t => t.DepartmentId)
                .ForeignKey("dbo.GeoLocations", t => t.GeoLocationId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .ForeignKey("dbo.Questionnaires", t => t.QuestionnaireId, cascadeDelete: true)
                .Index(t => t.OrganizationId)
                .Index(t => t.BusinessUnitId)
                .Index(t => t.QuestionnaireId)
                .Index(t => t.DepartmentId)
                .Index(t => t.GeoLocationId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Questionnaires",
                c => new
                    {
                        QuestionnaireId = c.Long(nullable: false, identity: true),
                        QuestionnaireTitle = c.String(),
                        Description = c.String(),
                        TimeNeeded = c.Int(nullable: false),
                        HasHint = c.Boolean(nullable: false),
                        WithReview = c.Boolean(nullable: false),
                        MandatoryAnswersToAllQuestions = c.Boolean(nullable: false),
                        NoOfQuestionsPerPage = c.Int(nullable: false),
                        WithResultPage = c.Boolean(nullable: false),
                        CalculateDistinctWeighting = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionnaireId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Long(nullable: false, identity: true),
                        FromUserId = c.Long(nullable: false),
                        ToUserId = c.Long(nullable: false),
                        MessageType = c.Int(nullable: false),
                        Subject = c.String(maxLength: 100),
                        MessageBody = c.String(),
                        DateSent = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateTable(
                "dbo.QuestionAnswers",
                c => new
                    {
                        QuestionAnswerId = c.Long(nullable: false, identity: true),
                        Lang = c.Int(nullable: false),
                        QuestionId = c.Long(nullable: false),
                        Description = c.String(),
                        MultipleChoiceValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionAnswerId)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        QuestionId = c.Long(nullable: false, identity: true),
                        QuestionTitle = c.String(),
                        QuestionCode = c.String(),
                        QuestionnaireId = c.Long(nullable: false),
                        QuestionSectorId = c.Long(),
                        Lang = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        DifficultyLevel = c.Int(nullable: false),
                        Weight = c.Int(nullable: false),
                        HasFeedback = c.Boolean(nullable: false),
                        HasHint = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionId)
                .ForeignKey("dbo.Questionnaires", t => t.QuestionnaireId, cascadeDelete: true)
                .ForeignKey("dbo.QuestionSectors", t => t.QuestionSectorId)
                .Index(t => t.QuestionnaireId)
                .Index(t => t.QuestionSectorId);
            
            CreateTable(
                "dbo.QuestionSectors",
                c => new
                    {
                        QuestionSectorId = c.Long(nullable: false, identity: true),
                        Code = c.String(),
                        QuestionSectorName = c.String(maxLength: 500),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionSectorId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserAnswers",
                c => new
                    {
                        UserAnswerId = c.Long(nullable: false, identity: true),
                        QuestionId = c.Long(nullable: false),
                        UserId = c.Long(),
                        SurveyId = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        FreeText = c.String(),
                        MultipleChoiceValue = c.Int(),
                    })
                .PrimaryKey(t => t.UserAnswerId)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.UserContactDetails",
                c => new
                    {
                        UserContactDetailId = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        Email = c.String(maxLength: 50),
                        Phone = c.String(maxLength: 20),
                        Mobile = c.String(maxLength: 20),
                        Country = c.String(maxLength: 100),
                        City = c.String(maxLength: 100),
                        Address = c.String(maxLength: 100),
                        Zip = c.String(maxLength: 10),
                        Facebook = c.String(maxLength: 250),
                        Twitter = c.String(maxLength: 250),
                        Google = c.String(maxLength: 250),
                        Linkedin = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.UserContactDetailId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserSurveys",
                c => new
                    {
                        UserSurveyId = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        SurveyId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserSurveyId)
                .ForeignKey("dbo.Surveys", t => t.SurveyId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SurveyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserSurveys", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserSurveys", "SurveyId", "dbo.Surveys");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserContactDetails", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserAnswers", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Questions", "QuestionSectorId", "dbo.QuestionSectors");
            DropForeignKey("dbo.Questions", "QuestionnaireId", "dbo.Questionnaires");
            DropForeignKey("dbo.QuestionAnswers", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Users", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Surveys", "QuestionnaireId", "dbo.Questionnaires");
            DropForeignKey("dbo.Surveys", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Surveys", "GeoLocationId", "dbo.GeoLocations");
            DropForeignKey("dbo.Surveys", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Surveys", "BusinessUnitId", "dbo.BusinessUnits");
            DropForeignKey("dbo.Surveys", "UserId", "dbo.Users");
            DropForeignKey("dbo.BusinessUnits", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Users", "GeoLocationId", "dbo.GeoLocations");
            DropForeignKey("dbo.Users", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Users", "BusinessUnitId", "dbo.BusinessUnits");
            DropForeignKey("dbo.Departments", "GeoLocationId", "dbo.GeoLocations");
            DropForeignKey("dbo.Departments", "BusinessUnitId", "dbo.BusinessUnits");
            DropIndex("dbo.UserSurveys", new[] { "SurveyId" });
            DropIndex("dbo.UserSurveys", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.UserContactDetails", new[] { "UserId" });
            DropIndex("dbo.UserAnswers", new[] { "QuestionId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Questions", new[] { "QuestionSectorId" });
            DropIndex("dbo.Questions", new[] { "QuestionnaireId" });
            DropIndex("dbo.QuestionAnswers", new[] { "QuestionId" });
            DropIndex("dbo.Surveys", new[] { "UserId" });
            DropIndex("dbo.Surveys", new[] { "GeoLocationId" });
            DropIndex("dbo.Surveys", new[] { "DepartmentId" });
            DropIndex("dbo.Surveys", new[] { "QuestionnaireId" });
            DropIndex("dbo.Surveys", new[] { "BusinessUnitId" });
            DropIndex("dbo.Surveys", new[] { "OrganizationId" });
            DropIndex("dbo.Users", new[] { "GeoLocationId" });
            DropIndex("dbo.Users", new[] { "BusinessUnitId" });
            DropIndex("dbo.Users", new[] { "DepartmentId" });
            DropIndex("dbo.Users", new[] { "OrganizationId" });
            DropIndex("dbo.Departments", new[] { "GeoLocationId" });
            DropIndex("dbo.Departments", new[] { "BusinessUnitId" });
            DropIndex("dbo.BusinessUnits", new[] { "OrganizationId" });
            DropTable("dbo.UserSurveys");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.UserContactDetails");
            DropTable("dbo.UserAnswers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.QuestionSectors");
            DropTable("dbo.Questions");
            DropTable("dbo.QuestionAnswers");
            DropTable("dbo.Messages");
            DropTable("dbo.Questionnaires");
            DropTable("dbo.Surveys");
            DropTable("dbo.Organizations");
            DropTable("dbo.Users");
            DropTable("dbo.GeoLocations");
            DropTable("dbo.Departments");
            DropTable("dbo.BusinessUnits");
        }
    }
}
