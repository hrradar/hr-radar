namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userContactDetailsRelation : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.UserContactDetails");
            DropForeignKey("dbo.UserContactDetails", "UserId", "dbo.Users");
            DropIndex("dbo.UserContactDetails", new[] { "UserId" });
            DropColumn("dbo.UserContactDetails", "UserContactDetailId");
            RenameColumn(table: "dbo.UserContactDetails", name: "UserId", newName: "UserContactDetailId");        
            AlterColumn("dbo.UserContactDetails", "UserContactDetailId", c => c.Long(nullable: false));
            AddPrimaryKey("dbo.UserContactDetails", "UserContactDetailId");
            CreateIndex("dbo.UserContactDetails", "UserContactDetailId");
            AddForeignKey("dbo.UserContactDetails", "UserContactDetailId", "dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserContactDetails", "UserContactDetailId", "dbo.Users");
            DropIndex("dbo.UserContactDetails", new[] { "UserContactDetailId" });
            DropPrimaryKey("dbo.UserContactDetails");
            AlterColumn("dbo.UserContactDetails", "UserContactDetailId", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.UserContactDetails", "UserContactDetailId");
            RenameColumn(table: "dbo.UserContactDetails", name: "UserContactDetailId", newName: "UserId");
            AddColumn("dbo.UserContactDetails", "UserContactDetailId", c => c.Long(nullable: false, identity: true));
            CreateIndex("dbo.UserContactDetails", "UserId");
            AddForeignKey("dbo.UserContactDetails", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
        }
    }
}
