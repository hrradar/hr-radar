namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionSectors", "QuestionSectorHeader", c => c.String());
            AddColumn("dbo.QuestionSectors", "ParticipatesInCalculation", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuestionSectors", "ParticipatesInCalculation");
            DropColumn("dbo.QuestionSectors", "QuestionSectorHeader");
        }
    }
}
