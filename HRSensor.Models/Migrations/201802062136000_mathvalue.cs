namespace HRSensor.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mathvalue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionAnswers", "MathValue", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuestionAnswers", "MathValue");
        }
    }
}
