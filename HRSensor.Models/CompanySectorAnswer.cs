﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.Models
{
    public class CompanySectorAnswer
    {
        public long CompanySectorAnswerId { get; set; }
        public long QuestionSectorId { get; set; }
        public long OrganizationId { get; set; }
        public long SurveyId { get; set; }
        public float MathAvg { get; set; }
        public int CPRI { get; set; }
    }
}