﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRRadar.Calculator
{
    public class Calculator: ICalculator
    {
        private readonly ApplicationDbContext _dbContext;

        public Calculator(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<float> Compute()
        {
            float[] result = { 0.6f, 0.7f, 0.8f };
            return new List<float>(result);

        }
    }
}
