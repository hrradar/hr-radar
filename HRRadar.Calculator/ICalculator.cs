﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRRadar.Calculator
{
    public interface ICalculator
    {
        List<float> Compute();
    }
}
