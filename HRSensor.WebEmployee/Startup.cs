﻿using AutoMapper;
using HRSensor.Models;
using HRSensor.Repositories;
using HRSensor.Repositories.Interfaces;
using HRSensor.WebEmployee.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: OwinStartupAttribute(typeof(HRSensor.WebEmployee.Startup))]
namespace HRSensor.WebEmployee
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var services = new ServiceCollection();
            ConfigureAuth(app);
            ConfigureServices(services);

            var resolver = new DefaultDependencyResolver(services.BuildServiceProvider());
            DependencyResolver.SetResolver(resolver);

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<QuestionAnswer, QuestionAnswerViewModel>();
                cfg.CreateMap<Question, QuestionViewModel>();
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersAsServices(typeof(Startup).Assembly.GetExportedTypes()
                .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
                .Where(t => typeof(IController).IsAssignableFrom(t) || t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)));
            
            services.AddScoped<IUserRepo, UserRepo>();
            services.AddScoped<ISurveyRepo, SurveyRepo>();
            services.AddScoped<IQuestionnaireRepo, QuestionnaireRepo>();
        }
    }

    public class DefaultDependencyResolver : IDependencyResolver
    {
        protected IServiceProvider serviceProvider;

        public DefaultDependencyResolver(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public object GetService(Type serviceType)
        {
            return this.serviceProvider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.serviceProvider.GetServices(serviceType);
        }
    }

    public static class ServiceProviderExtensions
    {
        public static IServiceCollection AddControllersAsServices(this IServiceCollection services,
           IEnumerable<Type> controllerTypes)
        {
            foreach (var type in controllerTypes)
            {
                services.AddTransient(type);
            }

            return services;
        }
    }
}
