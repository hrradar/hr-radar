﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.WebEmployee.ViewModels
{
    public class QuestionAnswerViewModel
    {
        public long QuestionAnswerId { get; set; }
        public Lang Lang { get; set; }
        public long QuestionId { get; set; }
        public string Description { get; set; }
        public Weight MultipleChoiceValue { get; set; }
    }
}