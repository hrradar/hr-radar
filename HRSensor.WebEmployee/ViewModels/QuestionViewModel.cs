﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.WebEmployee.ViewModels
{
    public class QuestionViewModel
    {
        public long QuestionId { get; set; }
        public string QuestionCode { get; set; }
        public string QuestionTitle { get; set; }
        public Lang Lang { get; set; }
        public QuestionType Type { get; set; }
        public DifficultyLevel DifficultyLevel { get; set; }
        public Weight Weight { get; set; }
        public bool IsActive { get; set; } = true;
        public QuestionSector QuestionSector { get; set; }
        public ICollection<QuestionAnswerViewModel> QuestionAnswers { get; set; }
    }
}