﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.WebEmployee.ViewModels
{
    public class UserAnswersViewModel
    {
        public long QuestionId { get; set; }
        public QuestionType Type { get; set; }
        public string FreeText { get; set; }
        public Weight? MultipleChoiceValue { get; set; }
    }
}