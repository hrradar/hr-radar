﻿using HRSensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static HRSensor.Common.Enums;

namespace HRSensor.WebEmployee.ViewModels
{
    public class SurveyViewModel
    {
        public long SurveyId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public SurveyStatus Status { get; set; }
        public List<QuestionViewModel> Questions { get; set; }
    }
}