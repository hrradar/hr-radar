﻿using HRSensor.WebEmployee.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSensor.Models
{
    public class SurveyResultsViewModel
    {
        public long UserId { get; set; }
        public long SurveyId { get; set; }
        public List<UserAnswersViewModel> UserAnswers{ get; set; }
    }
}
