﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HRSensor.WebEmployee.Helpers
{
    public static class LanguageHelper
    {
        public static MvcHtmlString LangSwitcher(this UrlHelper url, string Name, RouteData routeData, string lang)
        {
            var aTagBuilder = new TagBuilder("a");
            var iTagBuilder = new TagBuilder("i");

            if (Name.Trim().Equals("English"))
            {
                iTagBuilder.AddCssClass("flag-icon flag-icon-gb");           
            }
            else if (Name.Trim().Equals("Greek"))
            {
                iTagBuilder.AddCssClass("flag-icon flag-icon-gr");
            }
            aTagBuilder.GenerateId("lang_" + lang);

            aTagBuilder.AddCssClass("dropdown-item");
            var routeValueDictionary = new RouteValueDictionary(routeData.Values);
            if (routeValueDictionary.ContainsKey("lang"))
            {
                if (routeData.Values["lang"] as string == lang)
                {
                    //liTagBuilder.AddCssClass("active");
                }
                else
                {
                    routeValueDictionary["lang"] = lang;
                }
            }
            aTagBuilder.MergeAttribute("href", url.RouteUrl(routeValueDictionary));            
            aTagBuilder.InnerHtml = (iTagBuilder + Name);
            //liTagBuilder.InnerHtml = aTagBuilder.ToString();
            return new MvcHtmlString(aTagBuilder.ToString());
        }
    }
}