﻿var Chart = {
    drawChart: function (chart_data) {
        var myChart = echarts.init(document.getElementById(chart_data.idelement));
        var app = {};
        option = null;

        switch (chart_data.type) {
            case 'cprs_history':
            case 'job_satisfaction_history':
                option = {

                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: chart_data.legend_data
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true }
                        }
                    },
                    color: ["#009efb"],
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: ['12/2017', '1/2018', '2/2018', '3/2018']
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            axisLabel: {
                                formatter: '{value}'
                            }
                        }
                    ],

                    series: [
                        {
                            name: chart_data.name,
                            type: 'line',
                            color: ['#000'],
                            data: chart_data.series_data,
                            itemStyle: {
                                normal: {
                                    lineStyle: {
                                        shadowColor: 'rgba(0,0,0,0.3)',
                                        shadowBlur: 10,
                                        shadowOffsetX: 8,
                                        shadowOffsetY: 8
                                    }
                                }
                            }
                        }
                    ]
                };
                break;
            case 'cprs':
                // specify chart configuration item and data
                option = {
                    tooltip: {
                        formatter: "{a} <br/>{b} : {c}"
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true }
                        }
                    },

                    series: [
                        {
                            name: 'Current CPRS',
                            type: 'gauge',
                            min: 0,
                            max: 20,
                            detail: { formatter: '{value}' },
                            data: chart_data.series_data,
                            axisLine: {
                                lineStyle: {
                                    color: [[0.1, '#55ce63'], [0.3, '#ffbf00'], [0.5, '#ff8000'], [1, '#f62d51']],
                                }
                            }
                        }
                    ]
                };

                break;
            case 'job_satisfaction':
                option = {
                    title: {
                        text: '',
                        subtext: '',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                        data: chart_data.legend_data
                    },
                    series: [
                        {
                            name: chart_data.name,
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '50%'],
                            data: chart_data.series_data,
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                break;
            case 'health_wellbeing':
                option = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['General Health Perception', 'Mental Health And Well Being']
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true }
                        }
                    },
                    color: ["#55ce63", "#009efb"],
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            data: ['12/2017', '1/2018', '2/2018', '3/2018']
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: 'General Health Perception',
                            type: 'bar',
                            data: [2.0, 4.9, 7.0, 23.2],
                            markPoint: {
                                data: [
                                    { type: 'max', name: 'Max' },
                                    { type: 'min', name: 'Min' }
                                ]
                            },
                            markLine: {
                                data: [
                                    { type: 'average', name: 'Average' }
                                ]
                            }
                        },
                        {
                            name: 'Mental Health And Well Being',
                            type: 'bar',
                            data: [2.6, 5.9, 9.0, 26.4],
                            markPoint: {
                                data: [
                                    { name: 'The highest year', value: 182.2, xAxis: 7, yAxis: 183, symbolSize: 18 },
                                    { name: 'Year minimum', value: 2.3, xAxis: 11, yAxis: 3 }
                                ]
                            },
                            markLine: {
                                data: [
                                    { type: 'average', name: 'Average' }
                                ]
                            }
                        }
                    ]
                };

                break;
        }

        if (option && typeof option === "object") {
            myChart.setOption(option, true), $(function () {
                function resize() {
                    setTimeout(function () {
                        myChart.resize()
                    }, 100)
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
            });
        }
    }
}

var cprs_chart = {};
cprs_chart.type = 'cprs';
cprs_chart.legend_data = ['CPRS'];
cprs_chart.series_data = [{ value: 8, name: 'CPRS' }];
cprs_chart.idelement = 'cprs-chart';
Chart.drawChart(cprs_chart);

var cprs_history_chart = {};
cprs_history_chart.type = 'cprs_history';
cprs_history_chart.legend_data = ['CPRS History'];
cprs_history_chart.name = 'CPRS';
cprs_history_chart.series_data = [10, 11, 11, 8];
cprs_history_chart.idelement = 'cprs-history-chart';
Chart.drawChart(cprs_history_chart);

var job_satisfaction_chart = {};
job_satisfaction_chart.type = 'job_satisfaction';
job_satisfaction_chart.legend_data = ['Yes', 'No'];
job_satisfaction_chart.name = 'Job Satisfaction';
job_satisfaction_chart.series_data = [
    { value: 200, name: 'Yes' },
    { value: 310, name: 'No' }
];
job_satisfaction_chart.idelement = 'job-satisfaction-chart';
Chart.drawChart(job_satisfaction_chart);

var job_satisfaction_hist_chart = {};
job_satisfaction_hist_chart.type = 'job_satisfaction_history';
job_satisfaction_hist_chart.legend_data = ['Job Satisfaction History'];
job_satisfaction_hist_chart.name = 'Job Satisfaction';
job_satisfaction_hist_chart.series_data = [1, 2, 1, 0];
job_satisfaction_hist_chart.idelement = 'job-satisfaction-history-chart';
Chart.drawChart(job_satisfaction_hist_chart);


var health_wellbeing_chart = {};
health_wellbeing_chart.type = 'health_wellbeing';
health_wellbeing_chart.legend_data = ['Job Satisfaction History'];
health_wellbeing_chart.name = 'Job Satisfaction';
health_wellbeing_chart.series_data = [1, 2, 1, 0];
health_wellbeing_chart.idelement = 'health-wellbeing-chart';
Chart.drawChart(health_wellbeing_chart);