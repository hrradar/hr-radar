﻿using AutoMapper;
using HRSensor.Common;
using HRSensor.Models;
using HRSensor.Repositories.Interfaces;
using HRSensor.WebEmployee.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using static HRSensor.Common.Enums;

namespace HRSensor.WebEmployee.Controllers
{
    [Authorize(Roles = Constants.EMPLOYEE_ROLE)]
    public class HomeController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IServiceProvider _serviceProvider;
        private readonly ISurveyRepo _surveyRepo;
        private readonly IUserRepo _userRepo;

        public HomeController(IServiceProvider serviceProvider, ISurveyRepo surveyRepo, IUserRepo userRepo) : base(serviceProvider, userRepo)
        {
            _serviceProvider = serviceProvider;
            _surveyRepo = surveyRepo;
            _userRepo = userRepo;
        }

        public ActionResult Index()
        {
            // Decide later if we need a different index page
            return RedirectToAction("Surveys", "Home");
        }

        // GET: surveys
        public ActionResult Surveys()
        {
            var user = GetUserByAspNetUserId();
            var surveys = _surveyRepo.GetUserSurveysByUserId(user.UserId);

            ViewBag.Notifications = _userRepo.GetMessagesByUserId(user.UserId);

            return View(surveys.ToList());
        }


        public ActionResult Question()
        {
            ViewBag.Notifications = _userRepo.GetMessagesByUserId(GetUserByAspNetUserId().UserId);
            return View();
        }

        public ActionResult Survey(int id)
        {
            var surveyQuestions = _surveyRepo.GetSurveyQuestionsBySurveyId(id, GetThreadLanguageId());
            if (surveyQuestions == null) return HttpNotFound();
            
            var vm = new SurveyViewModel
            {
                Questions = Mapper.Map<List<Question>, List<QuestionViewModel>>(surveyQuestions.ToList())
            };
            Mapper.AssertConfigurationIsValid();
            vm.SurveyId = id;

            ViewBag.UserId = GetUserByAspNetUserId().UserId;
            ViewBag.Notifications = _userRepo.GetMessagesByUserId(GetUserByAspNetUserId().UserId);

            return View(vm);
        }

        [HttpPost]
        public ActionResult SaveSurveyAnswers(SurveyResultsViewModel surveyResults)
        {
            if (surveyResults.UserAnswers == null)
            {
                return HttpNotFound();
            }
            
            foreach (UserAnswersViewModel userAnswer in surveyResults.UserAnswers)
            {
                if (userAnswer != null)
                {
                    UserAnswer uAnswer = new UserAnswer
                    {
                        UserId = surveyResults.UserId,
                        QuestionId = userAnswer.QuestionId,
                        SurveyId = surveyResults.SurveyId,
                        Type = userAnswer.Type,
                        FreeText = userAnswer.FreeText,
                        MultipleChoiceValue = userAnswer.MultipleChoiceValue
                    };
                    db.UserAnswers.Add(uAnswer);
                }
                
            }

            var userSurvey = db.UserSurveys.Where(c => c.SurveyId == surveyResults.SurveyId && c.UserId == surveyResults.UserId).FirstOrDefault();

            userSurvey.Status = SurveyStatus.Completed;            
            userSurvey.IsActive = false;
            var surveyId = surveyResults.SurveyId;

            var user = GetUserByAspNetUserId();
            var message = db.Messages.Where(m => m.RelatedId == surveyResults.SurveyId && m.ToUserId == user.UserId).FirstOrDefault();
            if (message != null)
            {
                db.Messages.Remove(message);
            }            

            db.SaveChanges();
            int remainingSurveysCounter = db.UserSurveys.Where(c => c.SurveyId == surveyResults.SurveyId && c.Status == 0).Count();
            if (remainingSurveysCounter == 0)
            {
                Survey sy = db.Surveys.Find(surveyResults.SurveyId);
                sy.Status = SurveyStatus.Completed;
                db.SaveChanges();
            }

            db.Database.ExecuteSqlCommand(
                                    "Calculate_AVG_MathValue_PerSector @UserId, @SurveyId",
                                    new SqlParameter("@UserId", surveyResults.UserId),
                                    new SqlParameter("@SurveyId", surveyResults.SurveyId));

            db.Database.ExecuteSqlCommand(
                                    "Calculate_AVG_MathValue_PerSector_Company @UserId, @SurveyId",
                                    new SqlParameter("@UserId", surveyResults.UserId),
                                    new SqlParameter("@SurveyId", surveyResults.SurveyId));

            db.Database.ExecuteSqlCommand(
                                    "Calculate_CPRI @UserId, @SurveyId",
                                    new SqlParameter("@UserId", surveyResults.UserId),
                                    new SqlParameter("@SurveyId", surveyResults.SurveyId));            

            return Json(new { result = "Redirect", url = Url.Action("Surveys", "Home") });
        }

    }
}
