﻿using HRSensor.Models;
using HRSensor.Repositories.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using static HRSensor.Common.Enums;

namespace HRSensor.WebEmployee.Controllers
{
    public abstract class BaseController : Controller
    {
        private string CurrentLanguageCode { get; set; }
        protected string aspNetUserId = System.Web.HttpContext.Current.User.Identity.GetUserId();
        private readonly IServiceProvider _serviceProvider;
        private readonly IUserRepo _userRepo;

        public BaseController(IServiceProvider serviceProvider, IUserRepo userRepo)
        {
            _serviceProvider = serviceProvider;
            _userRepo = _serviceProvider.GetService<IUserRepo>();
        }

        protected User GetUserByAspNetUserId() {
            return _userRepo.GetUserByAspNetUserId(aspNetUserId);
        }

        protected override void Initialize(RequestContext requestContext)
        {
            if (requestContext.RouteData.Values["lang"] != null && requestContext.RouteData.Values["lang"] as string != "null")
            {
                CurrentLanguageCode = (string)requestContext.RouteData.Values["lang"];
                if (CurrentLanguageCode != null)
                {
                    try
                    {
                        Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo(CurrentLanguageCode);
                    }
                    catch (Exception)
                    {
                        throw new NotSupportedException($"Invalid language code '{CurrentLanguageCode}'.");
                    }
                }
            }
            base.Initialize(requestContext);
        }

        protected Lang GetThreadLanguageId()
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;

            if (culture == "el")
            {
                return Lang.GR;
            }

            return Lang.EN;
        }
    }
}